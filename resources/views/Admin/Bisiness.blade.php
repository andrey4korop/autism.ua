<bisinessedit :model='{!! json_encode($model->data) !!}'
              :regionsdata='{!! json_encode(\App\Region::all()) !!}'
              :citysdata='{!! json_encode(\App\City::all()) !!}'
              :categorydata='{!! json_encode(\App\CategoryBisiness::all()->keyBy('id')) !!}'
              :podcategoriidata='{!! json_encode(\App\PodCategorii::all()->keyBy('id')) !!}'
    ></bisinessedit>
