<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Щотижнева розсилка | Аутизм Україна</title>
</head>
<body data-rsssl="1" style="cursor: auto; ">
<style type="text/css">	body{background-image:url('{{url('img/email/retina_dust.png')}}');background-repeat:repeat-y no-repeat;background-position:top center;}

    #outlook a{padding:0;}
    body{width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0!important;padding:0!important;}
    .ExternalClass{width:100%;}
    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
    .bodytbl{margin:0;padding:0;width:100% !important;}
    img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;display:block;max-width:100%;}
    a img{border:none;}
    p{margin:1em 0;}

    table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}
    table td{border-collapse:collapse;}
    .o-fix table,.o-fix td{mso-table-lspace:0pt;mso-table-rspace:0pt;}

    body,.bodytbl{background-color:#ffffff/*Background Color*/;}
    table{font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#585858;}
    td,p{line-height:24px;color:#585858/*Text*/;}
    td,tr{padding:0;}
    ul,ol{margin-top:24px;margin-bottom:24px;}
    li{line-height:24px;}

    a{color:#5ca8cd/*Contrast*/;text-decoration:none;padding:2px 0px;}
    a:link{color:#5ca8cd;}
    a:visited{color:#5ca8cd;}
    a:hover{color:#5ca8cd;}

    .h1{font-family:Helvetica,Arial,sans-serif;font-size:26px;letter-spacing:-1px;margin-bottom:16px;margin-top:2px;line-height:30px;}
    .h2{font-family:Helvetica,Arial,sans-serif;font-size:20px;letter-spacing:0;margin-top:2px;line-height:30px;}
    h1,h2,h3,h4,h5,h6{font-family:Helvetica,Arial,sans-serif;font-weight:normal;}
    h1{font-size:20px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px;line-height:24px;}
    h2{font-size:18px;margin-bottom:12px;margin-top:2px;line-height:24px;}
    h3{font-size:14px;margin-bottom:12px;margin-top:2px;line-height:24px;}
    h4{font-size:14px;font-weight:bold;}
    h5{font-size:13px;}
    h6{font-size:13px;font-weight:bold;}
    h1 a,h2 a,h3 a,h4 a,h5 a,h6 a{color:#5ca8cd;}
    h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active{color:#5ca8cd !important;}
    h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited{color:#5ca8cd !important;}

    .wrap.body,.wrap.header,.wrap.footer{background-color:#ffffff/*Body Background*/;}
    .padd{width:24px;}

    .small{font-size:11px;line-height:18px;}
    .separator{border-top:1px dotted #99cc66/*Separator Line*/;}
    .btn{margin-top:10px;display:block;}
    .btn img{display:inline;}
    .subline{line-height:18px;font-size:16px;letter-spacing:-1px;}

    table.textbutton td{background:#5ca8cd/*Text Button Background*/;padding:3px 14px 3px 14px;color:#585858;display:block;height:24px;border:1px solid #FEFEFE/*Text Button Border*/;vertical-align:top;margin-bottom:3px;border:1px solid #ccc;-webkit-border-radius:2px;border-radius:2px;margin-right:4px;margin-bottom:4px;}
    table.textbutton a{color:#585858;font-size:14px;font-weight:normal;line-height:14px;width:100%;display:inline-block;}

    table.textbutton a{color:#585858;font-size:14px;font-weight:normal;line-height:14px;width:100%;display:inline-block;}

    div.preheader{line-height:0px;font-size:0px;height:0px;display:none !important;display:none;visibility:hidden;}

    @media only screen and (max-width: 599px) {
        body{-webkit-text-size-adjust:120% !important;-ms-text-size-adjust:120% !important;}
        table{font-size:15px;}
        .subline{float:left;}
        .padd{width:12px !important;}
        .wrap{width:96% !important;}
        .wrap table{width:100% !important;}
        .wrap img{max-width:100% !important;height:auto !important;}
        .wrap .s{width:100% !important;}
        .wrap .m-0{width:0;display:none;}
        .wrap .m-b{margin-bottom:24px !important;}
        .wrap .m-b,.m-b img{display:block;min-width:100% !important;width:100% !important;}
        table.textbutton td{height:auto !important;padding:8px 14px 8px 14px !important;}
        table.textbutton a{font-size:18px !important;line-height:26px !important;}
    }
    @media only screen and (max-width: 479px) {
    }
    @media only screen and (max-width: 320px) {
    }
    @media only screen and (min-device-width: 375px) and (max-device-width: 667px) {
        body{-webkit-text-size-adjust:170% !important;-ms-text-size-adjust:170% !important;}
    }
    @media only screen and (min-device-width: 414px) and (max-device-width: 736px) {
        body{-webkit-text-size-adjust:170% !important;-ms-text-size-adjust:170% !important;}
    }

</style>
<table class="bodytbl" width="100%" cellpadding="0" cellspacing="0"><tbody><tr>
        <td background="{{url('/img/email/retina_dust.png')}}" align="center">
            <table width="600" cellpadding="0" cellspacing="0" class="wrap header">
                <tbody><tr><td height="24" colspan="3"></td></tr>
                <tr>
                    <td width="24" class="padd">&nbsp;</td>
                    <td valign="top" align="center">
                        <table cellpadding="0" cellspacing="0" class="o-fix"><tbody><tr>
                                <td width="552" valign="top" align="left">
                                    <table cellpadding="0" cellspacing="0" align="left"><tbody><tr>
                                            <td width="264" class="small" align="left" valign="middle">
                                                <a target="_top" href="{{url('/')}}"><img src="{{url('/img/email/slider-2-200x344.png')}}" alt="" width="100" height="172" border="0" editable="" label="Logo" data-id="19" class=""></a>
                                            </td>
                                        </tr></tbody></table>
                                    <table cellpadding="0" cellspacing="0" align="right" class="m-b"><tbody><tr>
                                            <td width="264" height="24" align="right" valign="bottom">
                                                <div class="subline"><single label="Subline">Щотижнева розсилка новин</single></div>
                                            </td>
                                        </tr></tbody></table>
                                </td>

                            </tr></tbody></table>
                    </td>
                    <td width="24" class="padd">&nbsp;</td>
                </tr>
                <tr class="m-0"><td height="24" colspan="3"></td></tr>
                </tbody></table>
            <modules style="display: block;" class="ui-sortable"><module label="Separator" class="active ui-sortable-handle" style="display: block;">
                    <table width="600" cellpadding="0" cellspacing="0" class="wrap body">
                        <tbody><tr>
                            <td height="12" colspan="3"></td>
                        </tr>
                        <tr><td width="24" class="padd">&nbsp;</td>
                            <td width="552" align="center"><table cellpadding="0" cellspacing="0" class="separator"><tbody><tr><td width="552" height="23">&nbsp;</td></tr></tbody></table></td>
                            <td width="24" class="padd">&nbsp;</td>
                        </tr>
                        </tbody></table>
                </module>

        @php
        $conents = \App\ContentModel::getLastContent();
        //dd($conents);
        @endphp
                @foreach($conents as $conent)
                <module label="1/3 Image on the Left" auto="" class="active ui-sortable-handle" style="display: block;" data-tag="">
                    <table width="600" cellpadding="0" cellspacing="0" class="wrap body">
                        <tbody><tr>
                            <td height="12"></td>
                        </tr>
                        <tr><td valign="top" align="center">
                                <table cellpadding="0" cellspacing="0"><tbody><tr><td width="24" class="padd">&nbsp;</td>
                                        <td width="552" valign="top">
                                            <table cellpadding="0" cellspacing="0" align="left" class="m-b"><tbody><tr><td width="168" valign="top" align="left">
                                                        @if($conent->main_img)
                                                            <img src="{{url($conent->main_img, true)}}" height="84" alt="{{$conent->title}}" width="168" border="0">
                                                            @endif
                                                    </td>
                                                </tr></tbody></table><table cellpadding="0" cellspacing="0" align="right"><tbody><tr><td width="360" valign="top" align="left">
                                                        <h1><single label="Headline">{{$conent->title}}</single></h1>
                                                        <multi label="Body"></multi><div class="btn"><buttons class="ui-sortable"><table class="textbutton" align="left"><tbody><tr><td align="center" width="auto"><a target="_top" href="{{\App\UTMLink::getLink($conent->routeURL, $task->mail_compaing_id)}}">Читати далі</a></td></tr></tbody></table></buttons></div>
                                                    </td>
                                                </tr></tbody></table></td>

                                        <td width="24" class="padd">&nbsp;</td>
                                    </tr></tbody></table></td>
                        </tr>
                        <tr>
                            <td height="12"></td>
                        </tr>
                        </tbody></table>
                </module>
                @endforeach











                <module label="Separator" class="active ui-sortable-handle" style="display: block;">
                    <table width="600" cellpadding="0" cellspacing="0" class="wrap body">
                        <tbody><tr>
                            <td height="12" colspan="3"></td>
                        </tr>
                        <tr><td width="24" class="padd">&nbsp;</td>
                            <td width="552" align="center"><table cellpadding="0" cellspacing="0" class="separator"><tbody><tr><td width="552" height="23">&nbsp;</td></tr></tbody></table></td>
                            <td width="24" class="padd">&nbsp;</td>
                        </tr>
                        </tbody></table>
                </module></modules>
            <table width="600" cellpadding="0" cellspacing="0" class="wrap footer">
                <tbody><tr><td height="12" colspan="3"></td></tr>
                <tr>
                    <td width="24" class="padd">&nbsp;</td>
                    <td valign="top" align="center">
                        <table cellpadding="0" cellspacing="0" class="o-fix"><tbody><tr>
                                <td width="552" valign="top" align="left">
                                    <table cellpadding="0" cellspacing="0" align="left"><tbody><tr>
                                            <td width="360" valign="top" align="left" class="small m-b">
                                                <div><single label="Address"></single></div>
                                                <div><single label="Copyright">© {{\Carbon\Carbon::now()->format('Y')}}
                                                        <a href="{{url('/')}}">Аутизм Україна</a>, Всі права захищені </single></div>
                                            </td>
                                        </tr></tbody></table>
                                    <table cellpadding="0" cellspacing="0" align="right"><tbody><tr>
                                            <td width="168" valign="top" align="right" class="small">
                                                <div class="btn"><buttons class="ui-sortable">

                                                        <a target="_top" href="https://facebook.com/autisminUkraine" label="Facebook" class="ui-sortable-handle"><img src="{{url('img/email/facebook.png')}}" width="32" height="32" style="max-width:32px;max-height:32px;display:inline;" class="social" alt="Share this on Facebook"></a><a target="_top" href="https://youtube.com/channel/UC2u4xQPiE-6vUX4kDgtVQJQ" editable="" label="Youtube" class="ui-sortable-handle"><img src="{{url('img/email/youtube.png')}}" width="32" height="32" style="max-width:32px;max-height:32px;display:inline;" class="social" alt="Share this on Youtube"></a></buttons></div>
                                            </td>
                                        </tr></tbody></table>
                                </td>
                            </tr></tbody></table>
                    </td>
                    <td width="24" class="padd">&nbsp;</td>
                </tr>
                <tr><td height="24" colspan="3"></td></tr>
                </tbody>
            </table>
        </td>
    </tr></tbody></table>
</body></html>