@extends('main')
@section('hidden')
<div class="seo_hidden">
    @php
        // dd($PageView)
    @endphp

    <h1>{{$Title}}</h1>
    <nav>
        @foreach($menu as $men)
            <ul>
                @foreach($men as $item)
                <li><a href="{{$item->routeURL}}">{{$item->title}}</a></li>
                    @if($item->children)
                        @foreach($item->children as $ite)
                            <li><a href="{{$ite->routeURL}}">{{$ite->title}}</a></li>
                        @endforeach
                    @endif
                @endforeach
            </ul>
        @endforeach
    </nav>
    <div class="">{!! $setting['main_page_top_text'] !!}</div>
    <div class="content">
        @if($Content && count($Content)>1)
            @foreach($Content as $cont)
                @if($cont && count($cont)>1)
                    @foreach($cont as $con)
                        <div class="">
                            <h3><a href="{{$con->routeURL or ''}}">{{$con->title or ''}}</a></h3>
                            @if(property_exists($cont, 'main_img'))
                            <img src="{{ $con->main_img ? url($con->main_img) : $con->main_img}}" alt="">
                            @endif
                            <div class="description">{{$con->contentShort or ''}}</div>
                        </div>
                    @endforeach
                @elseif($cont)
                    <div class="">
                        <h3><a href="{{$cont->routeURL or ''}}">{{$cont->title or ''}}</a></h3>

                        @if(property_exists($cont, 'main_img'))
                        <img src="{{$cont->main_img ? url($cont->main_img) : $cont->main_img}}" alt="">
                        @endif
                        <div class="description">{{$cont->contentShort or ''}}</div>
                    </div>
                @endif
            @endforeach
        @elseif($Content)
            <div class="">
                <h2><a href="{{$Content->routeURL or ''}}">{{$Content->title or ''}}</a></h2>
                @if(property_exists($Content, 'main_img'))
                <img src="{{$Content->main_img ? url($Content->main_img) : $Content->main_img}}" alt="">
                @endif
                <div class="description">{!! $Content->content or ''!!}</div>
            </div>
        @endif
    </div>
    <div class="">{!! $setting['main_page_about_us_text'] !!}</div>
    <div class="">{!! $setting['help_text'] !!}</div>
</div>
    @endsection