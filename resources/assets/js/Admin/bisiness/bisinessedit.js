import bisinessedit from './bisinessedit.vue'
import * as LocationPicker from 'vue2-location-picker'
window.Vue.use(LocationPicker, {
    installComponents: true, // If true, create it globally
});

window.Vue.component('bisinessedit', bisinessedit);