
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import App from './App.vue'
//import Vue from 'vue'


Vue.component('HeaderComponent', require('./components/HeaderComponent.vue'));
Vue.component('FooterComponent', require('./components/FooterComponent.vue'));
Vue.component('MainPageComponent', require('./components/Pages/MainPageComponent.vue'));
Vue.component('ListPage', require('./components/Pages/ListPage.vue'));
Vue.component('Page', require('./components/Pages/Page.vue'));
Vue.component('Categories', require('./components/Pages/Categories.vue'));
Vue.component('CategoryVideo', require('./components/Pages/CategoryVideo.vue'));
Vue.component('PageVideo', require('./components/Pages/PageVideo.vue'));
Vue.component('CategoryPresentation', require('./components/Pages/CategoryPresentation.vue'));
Vue.component('PagePresentation', require('./components/Pages/PagePresentation.vue'));
Vue.component('CategoryFiles', require('./components/Pages/CategoryFiles.vue'));
Vue.component('AboutUs', require('./components/Pages/AboutUs.vue'));
Vue.component('ContactUs', require('./components/Pages/ContactUs.vue'));
Vue.component('RegBisnes', require('./components/Pages/RegBisnes.vue'));
Vue.component('Servives', require('./components/Pages/Servives.vue'));
Vue.component('SuggestPage', require('./components/Pages/SuggestPage.vue'));
Vue.component('BissinesPage', require('./components/Pages/BissinesPage.vue'));
Vue.component('Calendar', require('./components/Pages/Calendar.vue'));
Vue.component('CabinetsCategory', require('./components/Pages/CabinetsCategory.vue'));
Vue.component('CabinetsOneCategory', require('./components/Pages/CabinetsOneCategory.vue'));
Vue.component('CabinetForm', require('./components/Pages/CabinetForm.vue'));
Vue.component('HelpDesk', require('./components/Pages/HelpDesk.vue'));
Vue.component('HelpDeskOne', require('./components/Pages/HelpDeskOne.vue'));
Vue.component('HelpDeskUser', require('./components/Pages/HelpDeskUser.vue'));
Vue.component('FAQPageAll', require('./components/Pages/FAQPageAll.vue'));
Vue.component('FaqPage', require('./components/Pages/FaqPage.vue'));
Vue.component('FaqPageCabinetsCategory', require('./components/Pages/FaqPageCabinetsCategory.vue'));
Vue.component('FaqPageCabinet', require('./components/Pages/FaqPageCabinet.vue'));
Vue.component('TestCategory', require('./components/Pages/TestCategory.vue'));
Vue.component('TestOne', require('./components/Pages/TestOne.vue'));
Vue.component('showResetForm', require('./components/Pages/showResetForm.vue'));
Vue.component('CalendarPage', require('./components/Pages/CalendarPage.vue'));

/**
 *CardComponent
 */

Vue.component('VazhnoCard', require('./components/Cards/VazhnoCard.vue'));
Vue.component('VazhnoCardBig', require('./components/Cards/VazhnoCardBig.vue'));
/*Vue.component('NewsCard', require('./components/Cards/NewsCard.vue'));
Vue.component('NewsCardBig', require('./components/Cards/NewsCardBig.vue'));*/
Vue.component('BlogCard', require('./components/Cards/BlogCard.vue'));
Vue.component('BlogCardBig', require('./components/Cards/BlogCardBig.vue'));
Vue.component('Blog2Card', require('./components/Cards/Blog2Card.vue'));
Vue.component('CalendarCard', require('./components/Cards/CalendarCard.vue'));
Vue.component('UserHistoryCard', require('./components/Cards/UserHistoryCard.vue'));
Vue.component('ListCard', require('./components/Cards/ListCard.vue'));
Vue.component('MicroCard', require('./components/Cards/microCard.vue'));
Vue.component('NowInterestingCard', require('./components/Cards/NowInterestingCard.vue'));

/**
 * MiniComponents
 */
Vue.component('SocialButtons', require('./components/mimiComponents/SocialButtons.vue'));
Vue.component('CountButtons', require('./components/mimiComponents/CountButtons.vue'));
Vue.component('pagination', require('./components/mimiComponents/Pagination.vue'));
Vue.component('breadcrumbs', require('./components/mimiComponents/Breadcrumbs.vue'));
Vue.component('SubscribeFormInMainPage', require('./components/mimiComponents/SubscribeFormInMainPage.vue'));
Vue.component('SubscribeRightBlock', require('./components/mimiComponents/SubscribeRightBlock.vue'));
Vue.component('SendMaterial', require('./components/mimiComponents/SendMaterial.vue'));
Vue.component('TooltipItem', require('./components/mimiComponents/tooltipItem.vue'));
Vue.component('MapComponent', require('./components/mimiComponents/MapComponent.vue'));
Vue.component('HelpDeskAll', require('./components/mimiComponents/HelpDeskAll.vue'));
Vue.component('HelpDeskMy', require('./components/mimiComponents/HelpDeskMy.vue'));
Vue.component('AdminRow', require('./components/mimiComponents/AdminRow.vue'));
Vue.component('hashTagBlock', require('./components/mimiComponents/hashTagBlock.vue'));
Vue.component('interstingNowBlock', require('./components/mimiComponents/interstingNowBlock.vue'));
Vue.component('NowInterestingBlock', require('./components/mimiComponents/NowInterestingBlock.vue'));
Vue.component('FagRightPopularBock', require('./components/mimiComponents/FagRightPopularBock.vue'));
Vue.component('FagRightPopularNew', require('./components/mimiComponents/FagRightPopularNew.vue'));
Vue.component('FagRightPopularCategory', require('./components/mimiComponents/FagRightPopularCategory.vue'));
Vue.component('FaqRightFirstBlock', require('./components/mimiComponents/FaqRightFirstBlock.vue'));
Vue.component('ButtonRegBissines', require('./components/mimiComponents/ButtonRegBissines.vue'));
/**
 * BlockComponents
 */
Vue.component('RightBlock', require('./components/Block/RightBlock.vue'));
Vue.component('FAQRightBlock', require('./components/Block/FAQRightBlock.vue'));
Vue.component('HeaderCategory', require('./components/Block/HeaderCategory.vue'));
Vue.component('Registration', require('./components/Block/Registration.vue'));
Vue.component('Login', require('./components/Block/Login.vue'));
Vue.component('HelpDeskBlock', require('./components/Block/HelpDeskBlock.vue'));
Vue.component('PersonalArea', require('./components/Block/PersonalArea.vue'));
Vue.component('CommentsBlock', require('./components/Block/CommentsBlock.vue'));
Vue.component('ForgotPassword', require('./components/Block/ForgotPassword.vue'));
Vue.component('haveError', require('./components/Block/haveError.vue'));
/**
 * Microcomponent
 */
Vue.component('Donate', require('./components/svg/Donate.vue'));
Vue.component('Profile', require('./components/svg/Profile.vue'));
Vue.component('Search', require('./components/svg/Search.vue'));
Vue.component('Menu', require('./components/svg/Menu.vue'));
Vue.component('GooglePlus', require('./components/svg/GooglePlus.vue'));
Vue.component('GooglePlusSVG', require('./components/svg/GooglePlusSVG.vue'));
Vue.component('Viber', require('./components/svg/Viber.vue'));
Vue.component('WhatUP', require('./components/svg/WhatUP.vue'));
Vue.component('Email', require('./components/svg/Email.vue'));
Vue.component('Instagram', require('./components/svg/Instagram.vue'));
Vue.component('FaceBook', require('./components/svg/FaceBook.vue'));
Vue.component('FaceBookSVG', require('./components/svg/FaceBookSVG.vue'));
Vue.component('Twitter', require('./components/svg/Twitter.vue'));
Vue.component('Consult', require('./components/svg/Consult.vue'));
Vue.component('Document', require('./components/svg/Document.vue'));
Vue.component('Tests', require('./components/svg/Tests.vue'));
Vue.component('Servises', require('./components/svg/Servises.vue'));
Vue.component('Library', require('./components/svg/Library.vue'));
Vue.component('Like', require('./components/svg/Like.vue'));
Vue.component('Coment', require('./components/svg/Coment.vue'));
Vue.component('See', require('./components/svg/See.vue'));
Vue.component('Location', require('./components/svg/Location.vue'));
Vue.component('ArrowDown', require('./components/svg/ArrowDown.vue'));
Vue.component('ClockSVG', require('./components/svg/ClockSVG.vue'));
Vue.component('Mail', require('./components/svg/Mail.vue'));
Vue.component('Upload', require('./components/svg/Upload.vue'));
Vue.component('Friendship', require('./components/svg/Friendship.vue'));


const app = new Vue({
    el: '#app',
    render: h => h(App)
});
