import cyrillicToTranslit from "cyrillic-to-translit-js/CyrillicToTranslit";

window.bus = new window.Vue({
    data(){
        return {
            lang: {}
        }
    },
    methods:{
        dateString(LaravelDateString){
            let date = new Date(LaravelDateString);
            return date.getDate() +'.'+(date.getMonth()+1) +'.' + date.getFullYear();
        },
        timeString(LaravelDateString){
            let date = new Date(LaravelDateString);
            return date.getHours() +':'+date.getMinutes() +':'+date.getSeconds();
        },
        setTitle(title){
            document.title = title;
        },
        t(text){
            return cyrillicToTranslit().transform(text, '_')
        },
        toCol(ar, col=3){
            let array = [];
            for(let i=0;i<col;i++){
                array[i]=[];
            }
            ar.forEach(function(el, i) {
                array[i%col].push(el);
            });
            return array;
        },
        absoluteURL(relative) {
            if(relative) {
                let stack = location.origin.split("/");
                let parts = relative.split("/");
                for (var i = 0; i < parts.length; i++) {
                    if (parts[i] == ".")
                        continue;
                    if (parts[i] == "..")
                        stack.pop();
                    else
                        stack.push(parts[i]);
                }
                return stack.join("/");
            }else {
                return null;
            }
        }
    }
});