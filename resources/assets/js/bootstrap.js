window.Vue = require('vue');
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = require('jquery');
window.jQuery = window.$;

window.moment = require('moment');

// Require Froala Editor js file.

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key',
//     cluster: 'mt1',
//     encrypted: true
// });

require('./bus');

//Vue plugins

import vueTopprogress from 'vue-top-progress'
import VueYoutube from 'vue-youtube'
import Notifications from 'vue-notification'
import VueRecaptcha from 'vue-recaptcha'
import Autocomplete from 'v-autocomplete'
import FullCalendar from 'vue-full-calendar'
import 'fullcalendar/dist/locale/uk'
Vue.use(FullCalendar);

import VueAuthenticate from 'vue-authenticate'
import VueAxios from 'vue-axios'

Vue.use(VueAuthenticate, {
    baseUrl: 'https://new.autism.ua', // Your API domain

    providers: {
        facebook: {
            clientId: '1536464809986114',
            authorizationEndpoint: 'https://www.facebook.com/v3.0/dialog/oauth',
            redirectUri: 'https://new.autism.ua/auth/facebook' // Your client app URL
        },
        google: {
            clientId: '313698601233-obnbnh5bk2fcg6q9edbtsv9dbma1sa1k.apps.googleusercontent.com',
            redirectUri: 'https://new.autism.ua/auth/google' // Your client app URL
        }
    }
})

Vue.use(VueAxios, window.axios)

import { VueEditor, Quill } from 'vue2-editor'
Vue.component('vue-editor', VueEditor );

Vue.component('vue-recaptcha', VueRecaptcha);
Vue.use(vueTopprogress);
Vue.use(VueYoutube);
Vue.use(Notifications);
Vue.use(Autocomplete);

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCZN9sXkJRWlPt3hjozp8tfGwpdc-irmB8',
        libraries: 'places',
    },
});
import * as LocationPicker from 'vue2-location-picker'
Vue.use(LocationPicker, {
    installComponents: true, // If true, create it globally
});

import rate from 'vue-rate';
Vue.use(rate)