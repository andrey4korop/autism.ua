<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpDeskRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_desk_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->nullable();
            $table->text('text')->nullable();
            $table->integer('departure_id')->nullable();
            $table->text('files')->nullable();
            $table->integer('user_id');
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_desk_requests');
    }
}
