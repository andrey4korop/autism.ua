<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('token')->nullable();
            $table->integer('value')->nullable();
        });
        Schema::create('retesgables',function (Blueprint $table2){
            $table2->integer('rate_id');
            $table2->integer('rategable_id');
            $table2->string('rategable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
        Schema::dropIfExists('retesgables');
    }
}
