<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPublish extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tables = [
        'bisinesses',
        'blogs',
        'books',
        'cabinet_categories',
        'calendars',
        'category_bisinesses',
        'documents',
        'faq_categories',
        'faq_pages',
        'files',
        'it_is_importants',
        'news',
        'pages',
        'presentations',
        'specialists',
        'user_histories',
        'videos'
    ];
    public function up()
    {
        foreach ($this->tables as $tabl) {
            Schema::table($tabl, function ($table) {
                $table->boolean('publish')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $tabl) {
            Schema::table($tabl, function ($table) {
                $table->dropColumn('publish');
            });
        }
    }
}
