let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if(!mix.inProduction()){
    mix.options({
        processCssUrls: false
    });
    mix.js('resources/assets/js/app.js', 'public/js')
        //.js('resources/assets/js/Admin/hashtag/hashtag.js', 'public/js')
        .sass('resources/assets/sass/app.scss', 'public/css')
        .sourceMaps();
}
if(mix.inProduction()){
    mix.options({
        processCssUrls: true
    });
    mix.js('resources/assets/js/app.js', 'public/js')
        .js('resources/assets/js/bootstrap.js', 'public/js/vendor.js')
        .js('resources/assets/js/Admin/bisiness/bisinessedit.js', 'public/js')
        .js('resources/assets/js/Admin/caregoryadmin/categoryadmin.js', 'public/js')
        .js('resources/assets/js/Admin/podcategoriiadmin/PodCategoriiAdmin.js', 'public/js')
        .js('resources/assets/js/Admin/arrayString/arrayString.js', 'public/js')
        .js('resources/assets/js/Admin/rospisanie/rospisanie.js', 'public/js')
        .js('resources/assets/js/Admin/iconPicker/fontAwesomePicker.js', 'public/js')
        .js('resources/assets/js/Admin/hashtag/hashtag.js', 'public/js')
        .js('resources/assets/js/Admin/testEditor/testEditor.js', 'public/js')
        .sass('resources/assets/sass/app.scss', 'public/css')
        .sourceMaps().version();
}

mix.browserSync({
    proxy: 'autism',
    open: false,
});
//mix.copyDirectory('resources/assets/img', 'public/img');