<?php
use App\ErrorMessage;
use SleepingOwl\Admin\Model\ModelConfiguration;
use App\Log;

AdminSection::registerModel(ErrorMessage::class, function (ModelConfiguration $model) {
    $model->setTitle('ErrorMessage')->setAlias('ErrorMessage');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('url')->setLabel('Посилання'),
            AdminColumn::link('text')->setLabel('Текст'),
            AdminColumn::link('coment')->setLabel('Коментар'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {

        $tab1 = AdminForm::panel();

        $tab1->setItems([
            AdminFormElement::html(function ($v){
                echo  '<div class="form-group form-element-text "><label for="url" class="control-label">Посилання</label><a type="text" target="_blank" id="url" name="url" href="'.$v->url.'" readonly="readonly" class="form-control">'.$v->url.'</a></div>';
            }),
            AdminFormElement::html(function ($v){
                echo  '<div class="form-group form-element-text "><label for="url" class="control-label">Текст</label> <div type="text" readonly="readonly" class="form-control">'.$v->text.'</div></div>';
            }),
            AdminFormElement::text('coment', 'Коментар')->setReadonly(true),
        ]);

        $tab1
            ->getButtons()->setButtons(['delete' => new \SleepingOwl\Admin\Form\Buttons\Delete(),]);
        return $tab1;
    });
    $model->updating(function ($config, $model){
        $before = [];
        $after = [];
        collect($model->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        Log::create(['data_before' => $before, 'data_after'=> $after,  'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'ErrorMessage']);
    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});