<?php
use App\BottomMenu;
use App\TypeModel;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(BottomMenu::class, function (ModelConfiguration $model) {
    $model->setTitle('Нижнє меню')->setAlias('BottomMenu');
    $model->onDisplay(function () {
        $display = AdminDisplay::tree();
        //$display->with('title');
        $display->setValue('title');
        //->setParentField('parent_id')
        //->setOrderField('order');
        $display->setFilters(
            AdminDisplayFilter::scope('left') // ?latest
        );
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $t = 'App\Helpers\RootModel';
        $selectOptions = \App\TypeModel::all()->mapWithKeys(function ($item){
            return [$item->modelName =>  $item->title];
        });
        $form = AdminForm::panel();
        $form->setItems(
            AdminFormElement::text('title', 'Заголовок')
                ->required(),
            AdminFormElement::select('menutable_type', 'Тип', $selectOptions->toArray())
            ->setSortable(false)
                ->required(),
               // ->setModelForOptions(TypeModel::class, 'title'),
            AdminFormElement::dependentselect('menutable_id', 'Сторінка')
                ->setModelForOptions($t, 'title')
                ->setDataDepends(['menutable_type'])
                ->required()
                ->setLoadOptionsQueryPreparer(function($item, $query) {
                   // dump($item);
                    if($item->getDependValue('menutable_type')) {
                        $t = $item->getDependValue('menutable_type');
                        $r = $t::find(1);
                        //dump($r->getTable());
                        return DB::table($r->getTable());
                    }else{
                        return $query;
                    }
                })
        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        return $form;
    });


    $model->creating(function ($config, $model){
        $model->category = 'Bottom';

    });
    $model->updating(function ($config, $model){
        $before = [];
        $after = [];
        collect($model->first()->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        \App\Log::create(['data_before' => $before, 'data_after'=> $after, 'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'BottomMenu']);
    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');
    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');
    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');
    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');
});