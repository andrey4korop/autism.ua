<?php

use SleepingOwl\Admin\Navigation\Page;

return [

    [
        'title' => 'Меню',
        'icon' => 'fa fa-object-group',
        'pages' => [
            (new Page(\App\TopMenu::class))
                ->setIcon('fa fa-rss-square')->addAlias('Menu')
                ->setPriority(1),
            (new Page(\App\LeftMenu::class))
                ->setIcon('fa fa-rss-square')->addAlias('LeftMenu')
                ->setPriority(2),
            (new Page(\App\BottomMenu::class))
                ->setIcon('fa fa-rss-square')->addAlias('BottomMenu')
                ->setPriority(3),

        ],
        'priority' => 43,
    ],
    [
        'title' => 'Контент',
        'icon' => 'fa fa-object-group',
        'pages' => [
            (new Page(\App\Page::class))
                ->setIcon('fa fa-file-text-o')
                ->setPriority(1),
            (new Page(\App\Blog::class))
                ->setIcon('fa fa-rss-square')
                ->setPriority(1),
            (new Page(\App\News::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(2),
            (new Page(\App\UserHistory::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(3),
            (new Page(\App\Calendar::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(4),
            (new Page(\App\ItIsImportant::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(5),
        ],
        'priority' => 44,
    ],
    [
        'title' => 'Матеріали',
        'icon' => 'fa fa-object-group',
        'pages' => [
            (new Page(\App\Video::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(1),
            (new Page(\App\Presentation::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(2),
            (new Page(\App\Files::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(3),
            (new Page(\App\Document::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(4),
            (new Page(\App\Book::class))
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(5),
        ],
        'priority' => 45,
    ],
    [
        'title' => 'Сторінки',
        'icon' => 'fa fa-object-group',
        'pages' => [
            (new Page())->setTitle('AboutUs')
                ->setIcon('fa fa-object-group')
                ->setUrl('/admin/AboutUs/1/edit')
                ->setPriority(1),
            (new Page())->setTitle('ContactUs')
                ->setIcon('fa fa-newspaper-o')
                ->setUrl('/admin/ContactUs/1/edit')
                ->setPriority(2),

        ],
        'priority' => 46,
    ],
    [
        'title' => 'Послуги',
        'icon' => 'fa fa-object-group',
        'pages' => [
            (new Page(\App\Bisiness::class))->setTitle('Список послуг')
                ->setIcon('fa fa-object-group')
                ->setPriority(1),
            (new Page(\App\CategoryBisiness::class))->setTitle('Категорії послуг')
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(2),
            (new Page(\App\PodCategorii::class))->setTitle('Підкатегорії')
                ->setIcon('fa fa-newspaper-o')
                ->setPriority(3),

        ],
        'priority' => 47,
    ],
    [
        'title' => 'Налаштування',
        'icon' => 'fa fa-object-group',
        'pages' => [
            (new Page(\App\Setting::class))->setTitle('Налаштування')
                ->setIcon('fa fa-object-group')
                ->setPriority(1),
            (new Page(\App\MainLangString::class))->setTitle('Текстові стрічки')
                ->setIcon('fa fa-object-group')
                ->setPriority(2),

        ],
        'priority' => 48,
    ],
    [
        'title' => 'Кабінети',
        'icon' => 'fa fa-object-group',
        'pages' => [
            (new Page(\App\Specialist::class))
                ->setIcon('fa fa-file-text-o')
                ->setPriority(0),
            (new Page(\App\CabinetCategory::class))
                ->setIcon('fa fa-rss-square')
                ->setPriority(1),

        ]
        ,
        'priority' => 49,
    ],
    [
        'title' => 'Faq',
        'icon' => 'fa fa-object-group',
        'pages' => [
            (new Page(\App\FaqCategory::class))
                ->setIcon('fa fa-file-text-o')
                ->setPriority(0),
            (new Page(\App\FaqPage::class))
                ->setIcon('fa fa-file-text-o')
                ->setPriority(1),


        ],
        'priority' => 50,
    ],
    (new Page(\App\Test::class))->setIcon('fa fa-rss-square')->setPriority(100),
    (new Page(\App\UserMaterial::class))->setIcon('fa fa-rss-square')->setPriority(101),
    (new Page(\App\SeoForStaticPage::class))->setIcon('fa fa-rss-square')->setPriority(102),
    (new Page(\App\User::class))->setIcon('fa fa-rss-square')->setPriority(103),
    (new Page(\App\ErrorMessage::class))->setIcon('fa fa-rss-square')->setPriority(104),
    (new Page(\App\Log::class))->setIcon('fa fa-rss-square')->setPriority(105),
    (new Page())->setTitle('Server')->setIcon('fa fa-server')->setUrl('/admin/server')->setPriority(106),
    (new Page())->setTitle('Robots')->setIcon('fa fa-android')->setUrl('/admin/robots')->setPriority(107),
    (new Page())->setTitle('Logs')->setIcon('fa fa-files-o')->setUrl('/admin/logs')->setPriority(108)
];