<?php
use App\Test;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Support\Facades\Auth;
use App\Log;
AdminSection::registerModel(Test::class, function (ModelConfiguration $model) {
    $model->setTitle('Test')->setAlias('Test');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumnFilters([
            null,
            AdminColumnFilter::text()->setPlaceholder('Пошук по заголовку')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            null,
            null,

        ])->setPlacement('table.header');

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок'),
            AdminColumn::color('color', 'Колір'),
            AdminColumnEditable::checkbox('publish')->setLabel('Опублікований')->setWidth('10px'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $tab1 = AdminForm::panel();
        $tab1->setItems(
            AdminFormElement::columns()
                ->addColumn(function() {
                    return [
                        AdminFormElement::text('title', 'Заголовок')->required(),
                        AdminFormElement::view('Admin.testEditor', [], function ($a, $b){
                            $a->data = json_decode($b->input('question'));
                            //dd($a, $b->all());

                        })
                    ];
                })
                ->addColumn(function() {
                    return [
                        AdminFormElement::checkbox('publish', 'Опублікувати'),
                        AdminFormElement::datetime('publish_at', 'Опублікувати в:'),
                        AdminFormElement::text('url', 'Посилання'),
                        AdminFormElement::colorPicker('color', 'Колір'),
                    ];
                })
        );
        $tab1
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $tab2 = AdminForm::panel();
        $tab2->setItems(
[]
        );
        $tab2
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        Meta::addJs('admin-scripts', '/js/testEditor.js', 'admin-default');
        $page = AdminDisplay::tabbed();
        $page->appendTab($tab1,  'Основне');
        $page->appendTab($tab2,  'Питання');
        return $page;
    });
    $model->creating(function ($config, $model){
        if(!$model->url){
            $model->slug();
        };
    });
    $model->updating(function ($config, $model){
        if(!$model->url){
            $model->slug();
        };
        $before = [];
        $after = [];
        collect($model->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        Log::create(['data_before' => $before, 'data_after'=> $after,  'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'Test']);
    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});