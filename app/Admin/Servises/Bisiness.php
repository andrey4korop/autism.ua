<?php
use App\Bisiness;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Bisiness::class, function (ModelConfiguration $model) {
    $model->setTitle('Послуги')->setAlias('Bisiness');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('data->nameOrganization')->setLabel('Назва Лістингу'),
            AdminColumn::link('shortDescription')->setLabel('Описання'),
            AdminColumn::link('data->fio')->setLabel('Хто заповнив анкету'),
            AdminColumn::link('data->email2')->setLabel('Email для дзвінку')->setWidth('100px'),
            AdminColumnEditable::checkbox('publish')->setLabel('Опублікований'),

           /* AdminColumn::datetime('created_at')->setLabel('Створений')->setWidth('150px'),*/
        ]);
        $display->paginate(20);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::form()->addScript('custom-script','https://maps.googleapis.com/maps/api/js?key=AIzaSyCZN9sXkJRWlPt3hjozp8tfGwpdc-irmB8&libraries=places');
        Meta::addJs('admin-scripts', '/js/bisinessedit.js', 'admin-default');
        Meta::addJs('admin-scripts2', '/js/rospisanie.js', 'admin-scripts');
        $form->setItems(
            AdminFormElement::view('Admin.Bisiness', [], function ($a, $b){

                $obj = json_decode($b->input('form'));

                if($obj->file1){
                    $obj->file1 = \App\Http\Controllers\UploadImageController::saveFile($obj->file1);
                }
                if($obj->file2){
                    $tempArray = [];
                    foreach ($obj->file2 as $file) {
                        $tempArray[] = \App\Http\Controllers\UploadImageController::saveFile($file);
                    }
                    $obj->file2 = $tempArray;
                }
                $a->data = $obj;
            }),
            AdminFormElement::checkbox('publish', 'Опублікувати'),
            AdminFormElement::datetime('publish_at', 'Опублікувати в:')
        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        $form2 = AdminForm::form();
        $form2->setItems(
            AdminFormElement::view('Admin.Rospisanie', [], function ($a, $b){
                //dump($a);
                $a->rospisanie = json_decode($b->input('rospisanie'));
                //dd($b->input('form'));
            })
        );

        $tabs = AdminDisplay::tabbed();
        $tabs->appendTab($form,  'Інфо');
        $tabs->appendTab($form2,  'Розписання роботи');
        return $tabs;
    });
    $model->creating(function ($config, $model){
        if(!$model->url){
            $model->slug();
        };
    });
    $model->updating(function ($config, $model){
        $before = [];
        $after = [];
        if(Request::input('form')) {
            $obj = json_decode(Request::input('form'));
            collect((array)$model->data)->each(function ($v, $k) use (&$before, &$after, &$obj) {
                if (property_exists($obj,$k) && $obj->$k != $v && $k != 'file1' && $k != 'file2') {
                    $before[$k] = $v;
                    $after[$k] = $obj->$k;
                }
            });
        }
        \App\Log::create(['data_before' => $before, 'data_after'=> $after, 'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'Bisiness']);

    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});