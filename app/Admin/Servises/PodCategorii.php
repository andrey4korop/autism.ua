<?php
use App\PodCategorii;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(PodCategorii::class, function (ModelConfiguration $model) {
    $model->setTitle('Підкатегорії')->setAlias('PodCategorii');
    $model->onDisplay(function () {
        $display = AdminDisplay::table();

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок')->setWidth('500px'),
            AdminColumn::link('description')->setLabel('Описання'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::form();
        $form->setItems(
            AdminFormElement::text('title', 'Заголовок')->required(),
            AdminFormElement::text('description', 'Описання'),
            AdminFormElement::checkbox('multiselected', 'Можливість вибрати декілька')
        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        $form2 = AdminForm::form();
        $form2->setItems(
            AdminFormElement::view('Admin.PodCategorii', [], function ($a, $b){
                //dump($a);
                $a->data = $b->input('form');
                //dd($b->input('form'));
            })
        );


        $tabs = AdminDisplay::tabbed();
        $tabs->appendTab($form,  'Інфо');
        $tabs->appendTab($form2,  'Список елементів');
        Meta::addJs('admin-scripts', '/js/PodCategoriiAdmin.js', 'admin-default');
        return $tabs;



        //return $form;
    });

    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');

});