<?php
use App\CategoryBisiness;
use SleepingOwl\Admin\Model\ModelConfiguration;
use SleepingOwl\Admin\Form\Columns\Column;

AdminSection::registerModel(CategoryBisiness::class, function (ModelConfiguration $model) {
    $model->setTitle('Категорії')->setAlias('CategoryBisiness');
    $model->onDisplay(function () {
        $display = AdminDisplay::table();

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::image('marker_img')->setLabel('Маркер')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок'),
            AdminColumnEditable::checkbox('publish')->setLabel('Опублікований')->setWidth('10px'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $tab1 = AdminForm::panel();
        $col1 = new Column( [
            AdminFormElement::text('title', 'Заголовок')->required(),
            //AdminFormElement::wysiwyg('description', 'Описання')->setEditor('ckeditor')->required()
        ]);
        $col1->setHtmlAttribute('class', 'col-md-9');
        $col2 = new Column([
            AdminFormElement::checkbox('publish', 'Опублікувати'),
            AdminFormElement::datetime('publish_at', 'Опублікувати в:'),
            AdminFormElement::image('marker_img', 'Маркер')->required(),
            AdminFormElement::view('Admin.IconPicker', [], function ($a, $b){
                //  dd($b->input('icon'));
                $a->icon = $b->input('icon');
            }),
            //AdminFormElement::text('icon', 'Іконка')->required(),

        ]);
        $col2->setHtmlAttribute('class', 'col-md-3');
        $tab1->setItems(
                AdminFormElement::columns()->addColumn([$col1, $col2])
        );
        $tab1
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $tab2 = AdminForm::panel();
        $tab2->setItems(
            AdminFormElement::view('Admin.PodCategorii', [], function ($a, $b){
                //dump($a);
                $a->data = $b->input('form');
                //dd($b->input('form'));
            })
        );
        $tab2
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $page = AdminDisplay::tabbed();
        $page->appendTab($tab1,  'Основне');
        $page->appendTab($tab2,  'Форма');
        Meta::addJs('admin-scripts', '/js/categoryadmin.js', 'admin-default');
        Meta::addJs('admin-scripts1', '/js/fontAwesomePicker.js', 'admin-default');
        //Meta::addCss('admin-scripts1', '/js/font-awesome-picker.css', 'admin-default');
        return $page;
        //return $form;
    });

    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');

});