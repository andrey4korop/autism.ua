<?php
use App\CabinetCategory;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(CabinetCategory::class, function (ModelConfiguration $model) {
    $model->setTitle('Категорії')->setAlias('CabinetCategory');
    $model->onDisplay(function () {
        $display = AdminDisplay::table();

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок')->setWidth('500px'),
            AdminColumn::color('color', 'Колір'),
            AdminColumnEditable::checkbox('publish')->setLabel('Опублікований')->setWidth('10px'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel();
        $form->setItems(
            AdminFormElement::columns()
                ->addColumn(function() {
                    return [
                        AdminFormElement::text('title', 'Заголовок')->required(),
                        AdminFormElement::colorPicker('color', 'Колір'),
                    ];
                })
                ->addColumn(function() {
                    return [
                        AdminFormElement::checkbox('publish', 'Опублікувати'),
                        AdminFormElement::text('url', 'Посилання'),
                    ];
                })
        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        return $form;
    });
    $model->creating(function ($config, $model){
        if(!$model->url){
            $model->slug();
        };
    });
    $model->updating(function ($config, $model){
        if(!$model->url){
            $model->slug();
        };
        $before = [];
        $after = [];
        collect($model->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        \App\Log::create(['data_before' => $before, 'data_after'=> $after, 'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'Blog']);
    });

    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');

});