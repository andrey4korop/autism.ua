<?php
use App\Specialist;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Specialist::class, function (ModelConfiguration $model) {
    $model->setTitle('Спеціалісти')->setAlias('Specialist');
    $model->onDisplay(function () {
        $display = AdminDisplay::table();

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('name')->setLabel('Ім\'я'),
            AdminColumn::link('title')->setLabel('Заголовок')->setWidth('500px'),
            AdminColumnEditable::checkbox('publish')->setLabel('Опублікований')->setWidth('10px'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        Meta::addJs('admin-scripts', '/js/arrayString.js', 'admin-default');
        $form = AdminForm::panel();
        $users = \App\User::all();
        $fah = $users->filter(function ($user){
            return $user->role->where('role' , 'fah')->count();
        })->mapWithKeys(function ($item){
            return [$item->id => $item->name];
        });
        //dd($fah->toArray());
        $form->setItems(
             AdminFormElement::columns()
                ->addColumn(function() use ($fah){
                    return [
                        AdminFormElement::text('title', 'Заголовок')->required(),
                        AdminFormElement::select('user_id', 'Фахівець')
                            ->setOptions($fah->toArray())->setDisplay('name'),
                        AdminFormElement::text('name', 'Ім\'я')->required(),
                        AdminFormElement::wysiwyg('description', 'Опис')->setEditor('ckeditor')->required(),
                    ];
                })
                ->addColumn(function() {
                    return [
                        AdminFormElement::checkbox('publish', 'Опублікувати'),
                        AdminFormElement::multiselect('category', 'Катогорії', \App\CabinetCategory::class)->setDisplay('title'),
                        AdminFormElement::image('photo', 'Фото'),
                        AdminFormElement::view('Admin.arrayString', [], function ($model){
                            $model->data = Request::input('data');
                        })
                    ];
                })
        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        return $form;
    });
    $model->creating(function ($config, $model){

    });
    $model->updating(function ($config, $model){
        $before = [];
        $after = [];
        collect($model->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        \App\Log::create(['data_before' => $before, 'data_after'=> $after, 'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'Specialist']);
    });

    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');

});