<?php
use App\SeoForStaticPage;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Support\Facades\Auth;
use App\Log;
AdminSection::registerModel(SeoForStaticPage::class, function (ModelConfiguration $model) {
    $model->setTitle('SEO')->setAlias('SeoForStaticPage');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumnFilters([
            null,
            AdminColumnFilter::text()->setPlaceholder('Пошук по заголовку')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            null,
            null,
        ])->setPlacement('table.header');

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('key')->setLabel('key')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок'),
            AdminColumn::link('seo.title')->setLabel('Meta-Заголовок'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {

        $tab1 = AdminForm::panel();

        $keyElement = AdminFormElement::text('key', 'key')->required();
        if($id){
            $keyElement->setReadonly(true);
        }
        $titleElement =  AdminFormElement::text('title', 'Заголовок')->required();
        if($id){
            $titleElement->setReadonly(true);
        }

        $tab1->setItems([
                $titleElement,
                $keyElement,
                AdminFormElement::text('seo.title', 'Meta-Заголовок'),
                AdminFormElement::text('seo.keywords', 'Meta-keywords'),
                AdminFormElement::text('seo.description', 'Meta-description'),
            ]);

         $tab1
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        return $tab1;
    });
    $model->updating(function ($config, $model){
        $before = [];
        $after = [];
        collect($model->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        Log::create(['data_before' => $before, 'data_after'=> $after,  'user_id' => Auth::check() ? Auth::user()->id : 1, 'model' => 'SEO']);
    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});