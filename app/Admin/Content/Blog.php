<?php
use App\Blog;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Support\Facades\Auth;
use App\Log;
AdminSection::registerModel(Blog::class, function (ModelConfiguration $model) {
    $model->setTitle('Блог')->setAlias('Blog');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumnFilters([
            null,
            null,
            AdminColumnFilter::text()->setPlaceholder('Пошук по заголовку')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            AdminColumnFilter::text()->setPlaceholder('Пошук по тегам')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()->setPlaceholder('З дати')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            )->setTo(
                AdminColumnFilter::date()->setPlaceholder('По дату')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            ),
            null,

        ])->setPlacement('table.header');

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::image('main_img')->setLabel('Зображення')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок'),
            AdminColumn::lists('tags.name', 'Теги'),
            AdminColumn::datetime('created_at')->setLabel('Створений')->setWidth('150px'),
            AdminColumnEditable::checkbox('publish')->setLabel('Опублікований')->setWidth('10px'),

        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $tab1 = AdminForm::panel();
        $tab1->setItems(
            AdminFormElement::columns()
                ->addColumn(function() {
                    return [
                        AdminFormElement::text('title', 'Заголовок')->required(),
                        AdminFormElement::wysiwyg('content', 'Текст')->setEditor('ckeditor')->required(),
                    ];
                })
                ->addColumn(function() {
                    return [
                        AdminFormElement::checkbox('publish', 'Опублікувати'),
                        AdminFormElement::datetime('publish_at', 'Опублікувати в:'),
                        AdminFormElement::text('url', 'Посилання'),
                        AdminFormElement::image('main_img', 'Зображення'),
                        AdminFormElement::view('Admin.hashTag', [], function ($a, $b){
                            $obj = json_decode($b->input('hashTag'));
                            $a->save();
                            $a->syncTags($obj);
                        })
                    ];
                })
        );
        $tab1
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $tab2 = AdminForm::panel();
        $tab2->setItems(
            AdminFormElement::text('seo.title', 'Meta-Заголовок'),
            AdminFormElement::text('seo.keywords', 'Meta-keywords'),
            AdminFormElement::text('seo.description', 'Meta-description')
        );
        $tab2
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $page = AdminDisplay::tabbed();
        $page->appendTab($tab1,  'Основне');
        $page->appendTab($tab2,  'SEO');
        Meta::addJs('admin-scripts', '/js/hashtag.js', 'admin-default');
        return $page;
    });
    $model->creating(function ($config, $model){
        $model->author_id = \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1;
        if(!$model->url){
            $model->slug();
        };
    });
    $model->updating(function ($config, $model){
        if(!$model->url){
            $model->slug();
        };
        $before = [];
        $after = [];
        collect($model->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        Log::create(['data_before' => $before, 'data_after'=> $after,  'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'Blog']);
    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});