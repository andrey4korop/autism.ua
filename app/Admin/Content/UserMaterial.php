<?php
use App\UserMaterial;
use SleepingOwl\Admin\Model\ModelConfiguration;
AdminSection::registerModel(UserMaterial::class, function (ModelConfiguration $model) {
    $model->setTitle('Матеріал від користувачів')->setAlias('UserMaterial');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumnFilters([
            null,
            null,
            AdminColumnFilter::text()->setPlaceholder('Пошук по заголовку')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            null,
            AdminColumnFilter::text()->setPlaceholder('Пошук по тегам')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()->setPlaceholder('З дати')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            )->setTo(
                AdminColumnFilter::date()->setPlaceholder('По дату')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            ),
        ])->setPlacement('table.header');

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::image('main_img')->setLabel('Зображення')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок')->setWidth('500px'),
            AdminColumn::link('category.title')->setLabel('Категорія'),
            AdminColumn::lists('tags.name', 'Теги'),
            AdminColumn::datetime('created_at')->setLabel('Створений')->setWidth('150px'),
        ]);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) { //TODO

        $tab1 = AdminForm::panel();
        $tab1->setItems(
            AdminFormElement::columns()
                ->addColumn(function() {
                    return [
                        AdminFormElement::text('title', 'Заголовок')->required(),
                        AdminFormElement::wysiwyg('content', 'Текст')->setEditor('ckeditor')->required(),
                    ];
                })
                ->addColumn(function() {
                    return [
                        AdminFormElement::select('category_user_material_id', 'Категорія', \App\CategoryUserMaterial::class),
                        AdminFormElement::checkbox('publish', 'Опублікувати'),
                        AdminFormElement::datetime('publish_at', 'Опублікувати в:'),
                        AdminFormElement::text('url', 'Посилання'),
                        AdminFormElement::image('main_img', 'Зображення'),
                        AdminFormElement::view('Admin.hashTag', [], function ($a, $b){
                            $obj = json_decode($b->input('hashTag'));
                            $a->save();
                            $a->syncTags($obj);
                        })

                    ];
                })
        );
        $tab1
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $tab2 = AdminForm::panel();
        $tab2->setItems(
            AdminFormElement::text('seo.title', 'Meta-Заголовок'),
            AdminFormElement::text('seo.keywords', 'Meta-keywords'),
            AdminFormElement::text('seo.description', 'Meta-description')
        );
        $tab2
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $page = AdminDisplay::tabbed();
        $page->appendTab($tab1,  'Основне');
        $page->appendTab($tab2,  'SEO');
        Meta::addJs('admin-scripts', '/js/hashtag.js', 'admin-default');
        return $page;
    });
    $model->creating(function ($config, $model){
        if(!$model->url){
            $model->slug();
        };
    });
    $model->updating(function ($config, $model){

        if(!$model->url){
            $model->slug();
        };
        $content = new $model->category->model();
        $content->title = $model->title;
        $content->main_img = $model->main_img;
        $content->content = $model->content;
        $content->url = $model->url;

        $content->save();
        $model->delete();
        //dd($model);
        return true;

    });
    $model->setRedirect(['edit' => 'display']);
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});