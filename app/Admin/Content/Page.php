<?php
use App\Page;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Illuminate\Support\Facades\Auth;
use App\Log;
AdminSection::registerModel(Page::class, function (ModelConfiguration $model) {
    $model->setTitle('Сторінки')->setAlias('Page');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumnFilters([
            null,
            null,
            AdminColumnFilter::text()->setPlaceholder('Пошук по заголовку')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            AdminColumnFilter::text()->setPlaceholder('Пошук по тегам')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()->setPlaceholder('З дати')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            )->setTo(
                AdminColumnFilter::date()->setPlaceholder('По дату')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            ),
            null,
        ])->setPlacement('table.header');

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::image('main_img')->setLabel('Зображення')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок')->setWidth('500px'),
            AdminColumn::lists('tags.name', 'Теги'),
            AdminColumn::datetime('created_at')->setLabel('Створений')->setWidth('150px'),
            AdminColumnEditable::checkbox('publish')->setLabel('Опублікований')->setWidth('10px'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $tab1 = AdminForm::panel();
        $tab1->setItems(
            AdminFormElement::columns()
                ->addColumn(function() {
                    return [
                        AdminFormElement::text('title', 'Заголовок')->required(),
                        AdminFormElement::wysiwyg('content', 'Текст')->setEditor('ckeditor')->required(),
                    ];
                })
                ->addColumn(function() {
                    return [
                        AdminFormElement::checkbox('publish', 'Опублікувати'),
                        AdminFormElement::datetime('publish_at', 'Опублікувати в:'),
                        AdminFormElement::text('url', 'Посилання'),
                        AdminFormElement::image('main_img', 'Зображення'),
                        AdminFormElement::view('Admin.hashTag', [], function ($a, $b){
                            $obj = json_decode($b->input('hashTag'));
                            $a->save();
                            $a->syncTags($obj);
                        }),

                        AdminFormElement::checkbox('setting->seeNews', 'Транслювати новини')->setDefaultValue(true),
                        AdminFormElement::checkbox('setting->disableComments', 'Вимкнути коментарі')->setDefaultValue(false),
                        AdminFormElement::checkbox('setting->disableAds', 'Вимкнути показ реклами')->setDefaultValue(false),
                        AdminFormElement::checkbox('setting->sidebarInterestingNow', 'Сайтбар - показувати блок "Цікаво зараз"')->setDefaultValue(true),
                        AdminFormElement::checkbox('setting->sidebarPopularNow', 'Сайтбар - показувати блок "Популярне зараз"')->setDefaultValue(true),
                        AdminFormElement::checkbox('setting->sidebarComentNow', 'Сайтбар - показувати блок "Зараз обговорюють"')->setDefaultValue(true),
                        AdminFormElement::checkbox('setting->sidebarButtonNewMaterial', 'Сайтбар - показувати кнопку "Прилсати матеріал"')->setDefaultValue(true),
                        AdminFormElement::checkbox('setting->sidebarBlockPodpiska', 'Сайтбар - показувати форму "Підписатись на новини"')->setDefaultValue(true),

                    ];
                })
        );
        $tab1
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $tab2 = AdminForm::panel();
        $tab2->setItems(
            AdminFormElement::text('seo.title', 'Meta-Заголовок'),
            AdminFormElement::text('seo.keywords', 'Meta-keywords'),
            AdminFormElement::text('seo.description', 'Meta-description')
        );
        $tab2
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $page = AdminDisplay::tabbed();
        $page->appendTab($tab1,  'Основне');
        $page->appendTab($tab2,  'SEO');
        Meta::addJs('admin-scripts', '/js/hashtag.js', 'admin-default');
        return $page;
    });
    $model->creating(function ($config, $model){
        $model->author_id = \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1;
        if(!$model->url){
            $model->slug();
        };
    });
    $model->updating(function ($config, $model){
        if(!$model->url){
            $model->slug();
        };
        $before = [];
        $after = [];
        collect($model->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        Log::create(['data_before' => $before, 'data_after'=> $after, 'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'Page']);
    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});