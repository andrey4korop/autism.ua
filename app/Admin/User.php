<?php
use App\User;
use SleepingOwl\Admin\Model\ModelConfiguration;
AdminSection::registerModel(User::class, function (ModelConfiguration $model) {
    $model->setTitle('Користувачі')->setAlias('User');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumnFilters([
            null,
            AdminColumnFilter::text()->setPlaceholder('Пошук по імені')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            AdminColumnFilter::text()->setPlaceholder('Поиск по Email')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
        ])->setPlacement('table.header');

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('name')->setLabel('Ім\'я'),
            AdminColumn::link('email')->setLabel('Email')
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel();
        $form->setItems(
            AdminFormElement::text('name', 'Ім\'я')->required(),
            AdminFormElement::text('email', 'Email')->required(),
            AdminFormElement::multiselect('role', 'Права',\App\Role::class)->setDisplay('role_name')
        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        return $form;
    });
    $model->updating(function ($m, $r){

        $before = [];
        $after = [];
        collect($r->with('role')->first()->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        \App\Log::create(['data_before' => $before, 'data_after'=> $after, 'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'User']);
    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});