<?php
use App\Setting;
use SleepingOwl\Admin\Model\ModelConfiguration;
AdminSection::registerModel(Setting::class, function (ModelConfiguration $model) {
    $model->setTitle('Setting')->setAlias('Setting');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        /*$display->setColumnFilters([
            null,
            null,
            AdminColumnFilter::text()->setPlaceholder('Поиск по заголовку')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),

            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()->setPlaceholder('From Date')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            )->setTo(
                AdminColumnFilter::date()->setPlaceholder('To Date')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            ),
        ])->setPlacement('table.header');*/

        //$display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок')->setWidth('300px'),
            AdminColumn::text('value', 'Значение')->setWidth(100),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel();
        $formElement = '';
        if(!$id){
            $formElement = AdminFormElement::text('type', 'Тип')->required();
        }else{
            $setting = Setting::find($id);
            switch ($setting->type){
                case 'text':
                    $formElement = AdminFormElement::text('value', 'Значение')->required();
                    break;
                case 'textarea':
                    $formElement = AdminFormElement::wysiwyg('value', 'Значение')->setEditor('ckeditor')->required();
                    break;
                case 'number':
                    $formElement = AdminFormElement::number('value', 'Значение')->required();
                    break;
            }


        }
        $form->setItems(
            AdminFormElement::text('title', 'Заголовок')->required(),
            AdminFormElement::text('name', 'name')->required(),
            $formElement

        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        return $form;
    });

    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});