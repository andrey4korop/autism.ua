<?php
use App\MainLangString;
use SleepingOwl\Admin\Model\ModelConfiguration;
AdminSection::registerModel(MainLangString::class, function (ModelConfiguration $model) {
    $model->setTitle('MainLangString')->setAlias('MainLangString');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumnFilters([
            null,
            AdminColumnFilter::text()->setPlaceholder('Поиск по коду')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            AdminColumnFilter::text()->setPlaceholder('Поиск по тексту')->setOperator('contains')->setHtmlAttribute('style',  'width:90%'),

        ])->setPlacement('table.header');

        //$display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('shortCode')->setLabel('код')->setWidth(100),
            AdminColumnEditable::text('text', 'Значение')->setWidth('500px'),
        ]);
        $display->paginate(100);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel();
        $form->setItems(
            AdminFormElement::text('shortCode', 'код')->required(),
            AdminFormElement::text('text', 'Значение')->required()
        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        return $form;
    });

    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});