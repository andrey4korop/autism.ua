<?php
use App\Log;
use SleepingOwl\Admin\Model\ModelConfiguration;
AdminSection::registerModel(Log::class, function (ModelConfiguration $model) {
    $model->setTitle('Лог змін')->setAlias('Log');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumnFilters([
            null,
            AdminColumnFilter::text()->setPlaceholder('Поиск по пользователю')->setOperator('contains')->setHtmlAttribute('style',  'width:100%'),
            null,

            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()->setPlaceholder('From Date')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            )->setTo(
                AdminColumnFilter::date()->setPlaceholder('To Date')->setFormat('Y-m-d H:m')->setPickerFormat('d.m.Y H:m')
            ),
        ])->setPlacement('table.header');

        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('user.name')->setLabel('name'),
            AdminColumn::link('model')->setLabel('model'),
            AdminColumn::link('created_at')->setLabel('date')

        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel();

        //dd($role);
        $form->setItems(
            AdminFormElement::text('user.name', 'Ім\'я')->setReadonly(true),
            AdminFormElement::text('model', 'Модель')->setReadonly(true),
            AdminFormElement::html(function ($v){
                echo '<p>До</p>';
                dump($v->data_before);
            }),
            AdminFormElement::html(function ($v){
                echo '<p>После</p>';
                dump($v->data_after);
            })
        );
        $form
            ->getButtons()->setButtons([]);
        return $form;
    });
});