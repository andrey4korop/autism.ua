<?php
use App\CategoryUserMaterial;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(CategoryUserMaterial::class, function (ModelConfiguration $model) {
    $model->setTitle('CategoryUserMaterial')->setAlias('CategoryUserMaterial');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();
        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::link('title')->setLabel('Заголовок')->setWidth('500px'),
            AdminColumn::link('model')->setLabel('model')->setWidth('150px'),
        ]);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {

        $form = AdminForm::panel();
        $form->setItems(
            AdminFormElement::text('title', 'Заголовок'),
            AdminFormElement::text('model', 'model')
        );
        $form
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');
        return $form;
    });


    $model->creating(function ($config, $model){


    });
    $model->updating(function ($config, $model){

    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');
    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');
    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');
    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');
});