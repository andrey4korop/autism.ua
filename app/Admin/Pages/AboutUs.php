<?php
use App\AboutUs;
use SleepingOwl\Admin\Model\ModelConfiguration;
//Meta::setTitle('Test title') ->addJs('tghjgjgjhg','/js/app.js');
AdminSection::registerModel(AboutUs::class, function (ModelConfiguration $model) {
    $model->setTitle('AboutUs')->setAlias('AboutUs');
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync();


        $display->setHtmlAttribute('class', 'table-info table-hover');
        $display->setColumns([
            AdminColumn::text('id')->setLabel('#')->setWidth('20px'),
            AdminColumn::image('main_img')->setLabel('Зображення')->setWidth('20px'),
            AdminColumn::text('title')->setLabel('Заголовок')->setWidth('500px'),
        ]);
        $display->paginate(10);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function($id = null) {
        $tab1 = AdminForm::panel();
        $tab1->setItems(
            AdminFormElement::columns()
                ->addColumn(function() {
                    return [
                        AdminFormElement::text('title', 'Заголовок')->required(),
                        AdminFormElement::wysiwyg('content', 'Текст')->setEditor('ckeditor')->required(),
                    ];
                })
                ->addColumn(function() {
                    return [
                        //AdminFormElement::checkbox('publish', 'Опублікувати'),
                       // AdminFormElement::text('url', 'Посилання'),
                        AdminFormElement::image('main_img', 'Зображення'),
//AdminFormElement::yandexMap('position', 'Map'),
                        AdminFormElement::view('Admin.map', [], function ($d, $q){
                            $d->position = $q->input('loc');
                        }),
                    ];
                })
        );
        $tab1
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $tab2 = AdminForm::panel();
        $tab2->setItems(
            AdminFormElement::text('seo.title', 'Meta-Заголовок'),
            AdminFormElement::text('seo.keywords', 'Meta-keywords'),
            AdminFormElement::text('seo.description', 'Meta-description')
        );
        $tab2
            ->getButtons()
            ->setSaveButtonText('Зберегти')
            ->setDeleteButtonText('Видалити')
            ->setCancelButtonText('Відмінити');

        $page = AdminDisplay::tabbed();
        $page->appendTab($tab1,  'Основне');
        $page->appendTab($tab2,  'SEO');
        return $page;
    });
    $model->creating(function ($config, $model){

    });
    $model->updating(function ($config, $model){
        $before = [];
        $after = [];
        collect($model->toArray())->each(function ($v, $k) use (&$before, &$after){
            if(Request::input($k) && Request::input($k)!=$v){
                $before[$k] = $v;
                $after[$k] = Request::input($k);
            }
        });
        \App\Log::create(['data_before' => $before, 'data_after'=> $after, 'user_id' => \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->id : 1, 'model' => 'AboutUs']);
    });
    // Создание записи
    $model->setMessageOnCreate('Сторінка створена');

    // Редактирование записи
    $model->setMessageOnUpdate('Сторінка оновлена');

    // Удаление записи
    $model->setMessageOnDelete('Сторінка видалена');

    // Восстановление записи
    $model->setMessageOnRestore('Сторінка відновлена');


});