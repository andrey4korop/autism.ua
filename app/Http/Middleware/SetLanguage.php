<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->setting && Auth::user()->setting['lang']){
                app()->setLocale(Auth::user()->setting['lang']);
                if(!session('lang')) {
                    session(['lang' => Auth::user()->setting['lang']]);
                }
            }
        }elseif(session('lang')){
            app()->setLocale(session('lang'));
        }
        return $next($request);
    }
}
