<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadImageController extends Controller
{
    public function upload(Request $request){
        $data = $request->input('ImageData');
        $response = new \stdClass();
        $response->status = false;
        $response->url = '';
        if($data) {
            try {
                $data = explode( ',', $data );
                $datadecode = base64_decode($data[1]);
                $fileName = str_random(20) . '.png';
                Storage::put($fileName, $datadecode, 'public');
                $response->status = true;
                $response->url = Storage::url($fileName);
                return response()->json($response);
            }catch (\Exception $e){}
        }
        return response()->json($response);
    }
    public static function saveFile($data){
        if($data && !starts_with($data, '/')) {
            try {
                $data = explode(',', $data);
                $datadecode = base64_decode($data[1]);
                $fileName = str_random(20) . '.png';
                Storage::put($fileName, $datadecode, 'public');

            } catch (\Exception $e) {
                return null;
            }
            return Storage::url($fileName);
        }
        return $data;
    }
}
