<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Calendar;
use App\Comment;
use App\ItIsImportant;
use App\News;
use App\UserHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InterestingNowController extends Controller
{
    private $models = [
        Blog::class,
        News::class,
        UserHistory::class,
        Calendar::class,
        ItIsImportant::class
    ];
    public function getInterestingNow(){
        $colect = collect();
        foreach ($this->models as $model) {
            $colect->push($model::with('author')->where('created_at', '>', Carbon::now()->subMonth())->published()->get());
        }
        $colect = $colect->collapse();
        $colect = $colect->sortByDesc('counts.see')->values();
        return \MyResponse::good([
            'Content' => $colect,
        ]);
    }
    public function getLastComents(){
        //dd(Comment::orderByDesc('created_at')->has('commentable')->limit(100)->with(['user', 'commentable', 'commentable.author'])->get()->where('commentable', '!=', null)->unique('commentable.id')->take(3)->values());
        return \MyResponse::good([
            'Content' => Comment::orderByDesc('created_at')->has('commentable')->limit(100)->with(['user', 'commentable', 'commentable.author'])->get()->where('commentable', '!=', null)->unique('commentable.id')->take(3)->values(),
        ]);

    }
}
