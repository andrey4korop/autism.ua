<?php

namespace App\Http\Controllers;

use App\MainLangString;
use App\SeoForStaticPage;
use App\Test;
use Illuminate\Http\Request;
use MyResponse;

class TestController extends Controller
{
    public $adminPanelMenu=[];

    public function __construct()
    {
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Всі тести', 'url'=> route('admin.model', class_basename(Test::class))];
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Створити новий тест ', 'url'=> route('admin.model.create', class_basename(Test::class))];
    }
    public function index(){
        return MyResponse::good([
            'Title' => '',//MainLangString::getAll()[snake_case(class_basename(Test::class))],//TODO
            'category' => Test::published()->get(),
            'PageView' => 'TestCategory',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>strtolower(class_basename(Test::class))])->seo()->firstOrCreate([])->toArray(),
        ], $this->adminPanelMenu);
    }
    public function one(Request $request, Test $url){
        return MyResponse::good([
            'Title' => '',//MainLangString::getAll()[snake_case(class_basename(Test::class))],//TODO
            'Content' => $url,
            'PageView' => 'TestOne',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>strtolower(class_basename(Test::class))])->seo()->firstOrCreate([])->toArray(),
        ], $this->adminPanelMenu);
    }
}
