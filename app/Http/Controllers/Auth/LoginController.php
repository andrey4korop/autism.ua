<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\UserPhoto;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MikeMcLin\WpPassword\Facades\WpPassword;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), true
        );
    }
    public function sendLoginResponse(Request $request)
    {

        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        return response()->json(['status' => 'ok', 'user' => Auth::user()]);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $r = new \stdClass();
        $r->{$this->username()} = trans('auth.failed');

        return response()->json(['status' => 'error', 'messages' => $r]);
    }
    public function findOrCreateUser($oAuthUser)
    {
        $authUser = User::where('email', $oAuthUser->email)->first();
        if ($authUser) {
            return $authUser;
        }
        $user =  User::create([
            'name'     => $oAuthUser->name,
            'email'    => $oAuthUser->email,
            'password' => WpPassword::make($oAuthUser->id),
        ]);
        $photo = new UserPhoto();
        $photo->url = $oAuthUser->avatar_original;
        $user->photo()->save($photo);
        $role = Role::where('role', 'user')->first();
        $user->role()->attach($role);
        return $user;
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        //$request->session()->invalidate();
        return response()->json(['status' => 'ok', 'user' => '']);
    }
    public function redirectFacebook(Request $request){
        return Socialite::driver('facebook')->stateless()->redirect()->getTargetUrl();
    }
    public function facebook(Request $request){
        $oAuthUser = Socialite::driver('facebook')->stateless()->user();
        $authUser = $this->findOrCreateUser($oAuthUser);
        Auth::login($authUser, true);
        return response()->json(['status' => 'ok', 'user' => Auth::user()]);
    }
    public function google(Request $request){
        $oAuthUser = Socialite::driver('google')->stateless()->user();
        $authUser = $this->findOrCreateUser($oAuthUser);
        Auth::login($authUser, true);
        return response()->json(['status' => 'ok', 'user' => Auth::user()]);
    }
}
