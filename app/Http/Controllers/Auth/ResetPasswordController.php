<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use MyResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return MyResponse::good([
           'cmd' => 'showResetForm',
            'token' => $token,
            'email' => $request->email,
        ]);
        /*
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );*/
    }
    public function reset(Request $request)
    {
        if(count( Validator::make($request->toArray(), $this->rules())->getMessageBag()->getMessages())) {
            return MyResponse::good([
                'ErrorCode' => 56,
                'messages' => Validator::make($request->toArray(),  $this->rules())->getMessageBag()->getMessages(),
            ]);
        }

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
        }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($response)
            : $this->sendResetFailedResponse($request, $response);
    }
    protected function sendResetResponse($response)
    {
        return MyResponse::good([]);
    }
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return  MyResponse::good([
            'ErrorCode' => 55,
        ]);
    }
}
