<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.07.2018
 * Time: 04:43
 */

namespace App\Http\Controllers;


use App\CabinetCategory;
use App\HelpDeskMessage;
use App\HelpDeskRequest;
use App\MainLangString;
use App\SeoForStaticPage;
use App\Setting;
use App\Specialist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MyResponse;

class Cabinet
{
    public $Content = [];


    public function index(Request $request){

        return MyResponse::good([
            'Title' => MainLangString::getAll()['servises'],
            'category' => CabinetCategory::published()->get(),
            'Content' => '',
            'PageView' => 'CabinetsCategory',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'cabinets'])->seo()->firstOrCreate([])->toArray(),
        ]);
    }

    public function oneCategory(Request $request, CabinetCategory $category){

        return MyResponse::good([
            'Title' => $category->title,
            'Content' => $category->load('spesialist'),
            'PageView' => 'CabinetsOneCategory',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'cabinets'])->seo()->firstOrCreate([])->toArray(),//TODO
        ]);
    }

    public function form(Request $request, CabinetCategory $category, Specialist $id){

        return MyResponse::good([
            'Title' => Setting::getSetting('helpdesk_form_title').' '.$id->name,//TODO
            'category' => $category,
            'Content' => $id,
            'PageView' => 'CabinetForm',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'cabinets'])->seo()->firstOrCreate([])->toArray(),//TODO
        ]);
    }

    public function create(Request $request){
        if(Auth::user()) {
            $helpDeskRequest = new HelpDeskRequest();
            $helpDeskRequest->subject = $request->input('subject');
            $helpDeskRequest->text = $request->input('text');
            $helpDeskRequest->departure_id = $request->input('departure');
            $helpDeskRequest->user_id = Auth::user()->id;
            $helpDeskRequest->status = 1;
            if($request->file('files')){
                $tempArray = [];
                foreach ($request->file('files') as $file){
                    $tempArray[] = $file->move('files/helpdesk', str_random().$file->getClientOriginalName())->getPathname();
                }
                $helpDeskRequest->files = $tempArray;
            }
            $helpDeskRequest->save();
            $this->ErrorCode = 1;
        }else{
            $this->ErrorCode = 2;
        }
        return MyResponse::good([]);
    }

    public function helpDeskUserIndex(Request $request){
        return MyResponse::good([
            'Title' => Setting::getSetting('helpdesk_user_title'),//TODO
            'category' => '',
            'Content' =>'',
            'PageView' => 'HelpDeskUser',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'cabinets'])->seo()->firstOrCreate([])->toArray(),//TODO
        ]);
    }

    public function helpDeskIndex(Request $request){

        $content = '';
        if($request->input('get')=='all'){
            $content = HelpDeskRequest::with('user')->get();
        }
        if($request->input('get')=='my'){
            $content = Auth::user()->specialist->MyHelpdesk->load('user');
        }
        if($request->input('get')=='user'){
            $content = Auth::user()->specialist->MyHelpdesk->load('user');
        }
        return MyResponse::good([
            'category' => '',
            'Content' =>$content,
            'PageView' => 'HelpDesk',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'cabinets'])->seo()->firstOrCreate([])->toArray(),//TODO
        ]);

    }
    public function helpDeskOne(Request $request, HelpDeskRequest $id){
        return MyResponse::good([
            'Title' => Setting::getSetting('helpdesk_user_title').' '.$id->subject,
            'category' => '',
            'Content' => $id->load(['user', 'message', 'message.user', 'message.departure']),
            'PageView' => 'HelpDeskOne',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'cabinets'])->seo()->firstOrCreate([])->toArray(),//TODO
        ]);
    }

    public function sendMessage(Request $request, HelpDeskRequest $id){
        $message = new HelpDeskMessage();
        $message->text = $request->input('text');
        $message->departure_id = Auth::user()->specialist->id;
        if($request->file('files')){
            $tempArray = [];
            foreach ($request->file('files') as $file){
                $tempArray[] = $file->move('files/helpdesk', str_random().$file->getClientOriginalName())->getPathname();
            }
            $message->files = $tempArray;
        }
        $message->user_id = $id->user_id;
        $message->is_read = 0;
        $message->from_user = 1; // 0 - user; 1 - spesialist
        $id->message()->save($message);
        return MyResponse::good([]);
    }
    public function sendMessageUser(Request $request, HelpDeskRequest $id){
        $message = new HelpDeskMessage();
        $message->text = $request->input('text');
        //$message->departure_id = Auth::user()->specialist->id;
        if($request->file('files')){
            $tempArray = [];
            foreach ($request->file('files') as $file){
                $tempArray[] = $file->move('files/helpdesk', str_random().$file->getClientOriginalName())->getPathname();
            }
            $message->files = $tempArray;
        }
        $message->user_id = Auth::user()->id;
        $message->is_read = 0;
        $message->from_user = 0; // 0 - user; 1 - spesialist
        $id->message()->save($message);
        return MyResponse::good([]);
    }
}