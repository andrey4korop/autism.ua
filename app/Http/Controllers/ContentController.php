<?php

namespace App\Http\Controllers;

use App\Comment;
use App\MainLangString;
use App\News;
use App\Page;
use App\SeoForStaticPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MyResponse;

abstract class ContentController extends Controller
{
    protected $model;

    public $Content = [];
    public $adminPanelMenu=[];
    public $pageView = 'Page';

    public function __construct()
    {
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Всі '. class_basename($this->model), 'url'=> route('admin.model', class_basename($this->model))];
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Створити нову '. class_basename($this->model), 'url'=> route('admin.model.create', class_basename($this->model))];
    }

    public function like($url){
       return response()->json($this->model::where('url', $url)->first()->toogleLike());
   }
    public function show(){
        $this->Content = $this->model::with('author')->orderBy('id', 'dest')->published()->paginate(20);

        return MyResponse::good([
            'Title' => MainLangString::getAll()[snake_case(class_basename($this->model))],//TODO
            'Content' => $this->Content->all(),
            'PageView' => 'ContentAll',
            'other' => $this->model::orderBy('id', 'dest')->limit(4)->get(), //TODO
            'pagination' => [
                'total' => $this->Content->total(),
                'per_page' => $this->Content->perPage(),
                'current_page' => $this->Content->currentPage(),
                'last_page' => $this->Content->lastPage(),
                'from' => $this->Content->firstItem(),
                'to' => $this->Content->lastItem()
            ],
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>strtolower(class_basename($this->model))])->seo()->firstOrCreate([])->toArray(),
        ], $this->adminPanelMenu);
    }
    public function showone($url){
        $this->Content = $this->model::where('url', $url)->with('tags')->first();
        if(!$this->Content){
            abort(404);
        }
        if(\Illuminate\Support\Facades\Request::ajax()) {
            $this->Content->addSee();
        }
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Редагувати сторінку', 'url'=> route('admin.model.edit', ['adminModel'=>class_basename($this->model), 'adminModelId' => $this->Content->id])];
//dd($this->Content->seo);
        return MyResponse::good([
            'Title' => $this->Content->title,
             'Content' => $this->Content,
             'PageView' => $this->pageView,
             'other' => $this->model != Page::class ? $this->model::published()->orderBy('id', 'dest')->limit(4)->get() : News::published()->orderBy('id', 'dest')->limit(4)->get(),
            'SEO'=>$this->Content->seo ? $this->Content->seo()->firstOrCreate([])->toArray() : ['title'=>'','keywords'=>'','description'=>''],
         ], $this->adminPanelMenu);
    }
    public function comment($url){
        $this->Content = ($this->model != Page::class) ? $this->model::where('url', $url)->first()->load(['comments','comments.user'])->comments : null;
        return MyResponse::good([
            'Coments' => $this->Content,
        ]);
    }
    public function addComment(Request $request, $url){
        $this->Content =  $this->model::where('url', $url)->first();
        $this->Content->comments()->save(new Comment(['text' => $request->text, 'user_id' => Auth::user()->id]));

        return MyResponse::good([
            'Coments' => $this->Content->comments->load(['user']),
        ]);
    }
}
