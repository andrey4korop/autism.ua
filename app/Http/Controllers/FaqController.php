<?php

namespace App\Http\Controllers;

use App\CabinetCategory;
use App\FaqCategory;
use App\FaqPage;
use App\MainLangString;
use App\SeoForStaticPage;
use Illuminate\Http\Request;
use MyResponse;

class FaqController extends Controller
{
    public function index(){
        return MyResponse::good([
            'Title' => '', //MainLangString::getAll()['faq'],
            'category' => FaqCategory::published()->get()->load('faqs'),
            'Content' => FaqPage::orderBy('created_at', 'dest')->published()->first(),
            'PageView' => 'FAQPageAll',
        ]);
    }
    public function one(Request $request, FaqCategory $url){
        return MyResponse::good([
            'Title' => $url->title, //MainLangString::getAll()['faq'],
            'category' => '',
            'Content' => $url->faqs,
            'PageView' => 'FAQPageAll',
        ]);
    }
    public function page(Request $request, FaqPage $url){
        return MyResponse::good([
            'Title' => $url->title, //MainLangString::getAll()['faq'],
            'Content' => $url,
            'PageView' => 'FaqPage',
        ]);
    }

    public function category(Request $request){

        return MyResponse::good([
            'Title' => MainLangString::getAll()['servises'],
            'category' => CabinetCategory::published()->get(),
            'Content' => '',
            'PageView' => 'FaqPageCabinetsCategory',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'cabinets'])->seo()->firstOrCreate([])->toArray(),
        ]);
    }

    public function oneCategory(Request $request, CabinetCategory $category){

        return MyResponse::good([
            'Title' => $category->title,
            'Content' => $category->load('spesialist'),
            'PageView' => 'FaqPageCabinet',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'cabinets'])->seo()->firstOrCreate([])->toArray(),//TODO
        ]);
    }

    public function getPopular(Request $request){
        return MyResponse::good([
            'Content' => FaqPage::published()->get()->sortByDesc('see.count')->slice(0, 5),
        ]);
    }
    public function getNew(Request $request){
        return MyResponse::good([
            'Content' => FaqPage::published()->get()->sortByDesc('created_at')->slice(0, 5),
        ]);
    }
    public function getCategory(){
        return MyResponse::good([
            'category' => FaqCategory::published()->with('faqs')->get()->sortByDesc('faqsCount')->slice(0, 5),
        ]);
    }
}
