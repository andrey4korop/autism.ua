<?php

namespace App\Http\Controllers;

use App\Bisiness;
use App\CategoryBisiness;
use App\City;
use App\Comment;
use App\PodCategorii;
use App\Region;
use App\SeoForStaticPage;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MyResponse;

class ServisesController extends Controller
{
    public $ListContent = [];
    public $Content = [];
    public $btnReadAll = [];
    public $adminPanelMenu=[];

    public function registratonBusiness(Request $request){

        return MyResponse::good([
            'Title' => Setting::getSetting('registrationPage_Title'), //TODO
            'regions' => Region::all(),
            'citys' => City::all(),
            'category' => CategoryBisiness::published()->get()->keyBy('id'),
            'podCategorii' => PodCategorii::all()->keyBy('id'),
            'PageView' => 'RegBisnes',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'registratonBusiness'])->seo()->firstOrCreate([])->toArray(),
        ]);

    }
    public function registratonBusiness2(Request $request){
        $user_id=null;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }
        $arr = collect($request->all())->except('file1');
        $r= new \stdClass();
        foreach ($arr as $key=>$value){
            $r->$key = $value;
        }
        /*r->email = $request->input('email');
        $r->name = $request->input('name');
        $r->email2 = $request->input('email2');
        $r->description = $request->input('description');
        $r->website = $request->input('website');
        $r->phone = $request->input('phone');
        $r->video = $request->input('video');
        $r->facebook = $request->input('facebook');
        $r->twitter = $request->input('twitter');
        $r->instagram = $request->input('instagram');
        $r->region = $request->input('region');
        $r->location = $request->input('location');
        $r->l = $request->input('l');
        $r->category = $request->input('category');*/
        $r->file1 ='';
        if($r->file1){
            $r->file1 = UploadImageController::saveFile($request->input('file1'));
        }
        //$r->file2 = [];
        /* foreach ($request->input('file2') as $value) {
            // $r->file2[] = UploadImageController::saveFile($value);
         }*/
        $bisiness = new Bisiness();
        $bisiness->data = $r;
        $bisiness->user_id = $user_id;
        $bisiness->save();
        return response()->json($bisiness);
    }
    public function map(Request $request){
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Всі підприемства', 'url'=> '/admin/Bisiness'];
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Створити нове підприесмтво', 'url'=> route('admin.model.create', 'Bisiness')];
        return MyResponse::good([
            'Title' => Setting::getSetting('mapPage_Title'), //TODO
            'regions' => Region::all(),
            'city' => City::all(),
            'items' => Bisiness::with('category')->published()->get(),
            'category' => CategoryBisiness::published()->get(),
            'PageView' => 'Map',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'map'])->seo()->firstOrCreate([])->toArray(),
        ], $this->adminPanelMenu);
    }
    public function searchMap(Request $request){
        $city = $request->input('q_city');
        $category = $request->input('q_category');
        $region = $request->input('q_region');
        $Bisiness = Bisiness::with('category')->published()->get();
        // dump($Bisiness);
        if($city) {
            $Bisiness = $Bisiness->filter(function ($item) use ($city){
                if(property_exists($item->data, 'city')) {
                    return $item->data->city == $city;
                }else{
                    return false;
                }
            });
        }
        if($category){
            $Bisiness = $Bisiness->filter(function ($item) use ($category){
                //dd($item->data->category);
                if(property_exists($item->data, 'category')) {
                    return ($item->data->category == $category);
                }else{
                    return true;
                }
            });
        }
        if($region){
            $Bisiness = $Bisiness->filter(function ($item) use ($region){
                return $item->data->region == $region;
            });
        }
        //dump($Bisiness);
        return response()->json($Bisiness->values());
    }
    public function bissines(Request $request, Bisiness $id){
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Всі підприемства', 'url'=> '/admin/Bisiness'];
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Створити нове підприесмтво', 'url'=> route('admin.model.create', 'Bisiness')];
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Редагувати підприесмтво', 'url'=> route('admin.model.edit', ['adminModel'=>'Bisiness', 'adminModelId' => $id->id])];
        return MyResponse::good([
            'Title' => $id->data->nameOrganization,
            'Content' => $id,
            'PageView' => 'BissinesPage',
            'regions' => Region::all(),
            'city' => City::all()->keyBy('id'),
            'items' => Bisiness::published()->get(),
            'category' => CategoryBisiness::published()->get()->keyBy('id'),
            'podCategorii' => PodCategorii::all()->keyBy('id'),
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'map'])->seo()->firstOrCreate([])->toArray(),
        ], $this->adminPanelMenu);
    }
    public function setRate(Request $request, Bisiness $id){
        $id->setRate($request->input('value'));
        return MyResponse::good([]);
    }
    public function commentC(Bisiness $id){
        return MyResponse::good([
            'Coments' => $id->comments->load(['user']),
        ]);
    }
    public function addCommentC(Request $request, Bisiness $id){
        $id->comments()->save(new Comment(['text' => $request->text, 'user_id' => Auth::user()->id]));
        return MyResponse::good([
            'Coments' => $id->comments->load(['user']),
        ]);
    }
}
