<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\Bisiness;
use App\CategoryBisiness;
use App\City;
use App\ContactUs;
use App\ErrorMessage;
use App\MainLangString;
use App\MessagesFromContactUs;
use App\Page;
use App\PodCategorii;
use App\Region;
use App\SeoForStaticPage;
use App\Setting;
use App\Tag;
use Illuminate\Http\Request;
use App\Blog;
use App\Calendar;
use App\ItIsImportant;
use App\News;
use App\UserHistory;
use Illuminate\Support\Facades\Auth;
use MyResponse;

class MainController  extends Controller
{
    public $ListContent = [];
    public $Content = [];
    public $btnReadAll = [];
    public $adminPanelMenu=[];

   public function index(Request $request){

      // \App::setLocale('fr');
        $this->Content['Blog']= Blog::orderBy('created_at', 'dest')->with('tags')->limit(15)->published()->get();
        $this->ListContent[] = 'Blog';
        $this->btnReadAll['Blog']= route('blog');

       $this->Content['Calendar']= Calendar::orderBy('created_at', 'dest')->with('tags')->limit(3)->published()->get();
       $this->ListContent[] = 'Calendar';
       $this->btnReadAll['Calendar']= route('calendar');

       $this->Content['ItIsImportant']= ItIsImportant::orderBy('created_at', 'dest')->with('tags')->limit(3)->published()->get();
       $this->ListContent[] = 'ItIsImportant';
       $this->btnReadAll['ItIsImportant']= route('important');

       $this->Content['News']= News::orderBy('created_at', 'dest')->with('tags')->limit(16)->published()->get();
       $this->ListContent[] = 'News';
       $this->btnReadAll['News']= route('news');

       $this->Content['UserHistory']= UserHistory::orderBy('created_at', 'dest')->with('tags')->limit(3)->published()->get();
       $this->ListContent[] = 'UserHistory';
       $this->btnReadAll['UserHistory']= route('UserHistory');

       return MyResponse::good([
           'Title' => Setting::getSetting('main_Title'), //TODO
           'ListContent' => $this->ListContent,
           'btnReadAll' => $this->btnReadAll,
           'Content' => $this->Content,
           'PageView' => 'main',
           'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'main'])->seo()->firstOrCreate([])->toArray(),
       ]);
   }

   public function aboutUs(Request $request){
       $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Редагувати сторінку', 'url'=> '/admin/AboutUs/1/edit'];
       $content = AboutUs::first();
       return MyResponse::good([
           'Title' => $content->title,
           'Content' => $content,
           'PageView' => 'aboutUs',
           'SEO'=> $content->seo()->firstOrCreate([])->toArray(),
       ], $this->adminPanelMenu);
   }

    public function contactUs(Request $request){
        $this->adminPanelMenu['adminPanelMenu'][] = ['title'=>'Редагувати сторінку', 'url'=> '/admin/ContactUs/1/edit'];
        $content = ContactUs::first();
        return MyResponse::good([
            'Title' =>$content->title,
            'Content' => $content,
            'PageView' => 'contactUs',
            'SEO'=> $content->seo()->firstOrCreate([])->toArray(),
        ], $this->adminPanelMenu);
    }

    public function contactUsPut(Request $request){
       $user_id=null;
       if(Auth::check()){
           $user_id = Auth::user()->id;
       }
       $message = new MessagesFromContactUs();
       $message->message = $request->input('text');
       $message->user_id = $user_id;
       $message->save();
       return response()->json($message);
    }

    public function getPopularTags(){
       $tags = Tag::getPopularTag()->take(15);
        return MyResponse::good([
           'tags' => $tags,
        ], $this->adminPanelMenu);
    }
    public function getListItemFromTag(Request $request, $tag){
        $tag = Tag::where('slug', 'like', '%'.$tag.'%')->first();
        $Content = $tag->items();

        return MyResponse::good([
            'Title' => $tag->name,//TODO
            'Content' => $Content,
            'PageView' => 'ContentAll',
            'other' => [], //TODO
            'pagination' => [
                'total' => 0,//$Content->total(),
                'per_page' => 0,//$Content->perPage(),
                'current_page' => 0,//$Content->currentPage(),
                'last_page' => 0,//$Content->lastPage(),
                'from' => 0,//$Content->firstItem(),
                'to' => 0,//$Content->lastItem()
            ],
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>strtolower(class_basename(Tag::class))])->seo()->firstOrCreate([])->toArray(),
        ], $this->adminPanelMenu);
    }
    public function changeLang(Request $request){
       if(Auth::check()){
           Auth::user()->changeLang($request);
       }else{
           session(['lang'=>$request->lang]);
       }
       return response()->json([]);
    }
    public function sendError(Request $request){
        ErrorMessage::create(['url' => $request->url, 'text'=> $request->string, 'coment'=>$request->coment]);
        return response()->json([]);
    }
}
