<?php

namespace App\Http\Controllers;

use App\Bisiness;
use App\Blog;
use App\Book;
use App\Calendar;
use App\Document;
use App\FaqCategory;
use App\FaqPage;
use App\Files;
use App\ItIsImportant;
use App\News;
use App\Page;
use App\Presentation;
use App\Specialist;
use App\Test;
use App\UserHistory;
use App\Video;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $modelsForSearch = [
        //Bisiness::class,
        Blog::class,
        Book::class,
        Calendar::class,
        Document::class,
        FaqCategory::class,
        FaqPage::class,
        Files::class,
        ItIsImportant::class,
        News::class,
        Page::class,
        Presentation::class,
       // Specialist::class,
        Test::class,
        UserHistory::class,
        Video::class
        ];

    public function search(Request $request){
        $collect = collect();
        foreach ($this->modelsForSearch as $model){
            $collect->push($model::published()->search($request->q, 5, true)->limit(2)->get());
        }
        return response()->json($collect->collapse()->shuffle());
    }
}
