<?php

namespace App\Http\Controllers;

use App\MainLangString;
use Illuminate\Http\Request;
use App\Calendar;
use Illuminate\Support\Facades\Auth;
use MyResponse;

class CalendarController extends ContentController
{
    protected $model =  Calendar::class;
    public $pageView = 'CalendarPage';

    public function show()
    {
        $this->Content = $this->model::orderBy('id', 'dest')->get();

        return MyResponse::good([
            'Title' => MainLangString::getAll()['calendar'],//TODO
            'Content' => $this->Content->all(),
            'PageView' => 'Calendar',
        ], $this->adminPanelMenu);
    }
}
