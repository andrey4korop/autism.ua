<?php

namespace App\Http\Controllers;
use App\CategoryUserMaterial;
use App\SeoForStaticPage;
use App\Setting;
use App\UserMaterial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use MyResponse;

class SuggestController extends ContentController
{
    public function index(){
        return MyResponse::good([
            'Title' => Setting::getSetting('suggest_title'),
            'Category' => CategoryUserMaterial::all(),
            'PageView' => 'SuggestPage',
            'SEO'=> SeoForStaticPage::firstOrCreate(['key'=>'suggest'])->seo()->firstOrCreate([])->toArray(),
        ]);
    }

    public function save(Request $request){
        //dump($request->all());
        $material = new UserMaterial($request->all());
        if($request->input('img')) {
            $material->main_img = UploadImageController::saveFile($request->input('img'));
        }
        $material->save();
        return MyResponse::good([]);
    }
}
