<?php

namespace App\Http\Controllers;

use App\Comment;
use App\TypeCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\ContentController;
use Illuminate\Support\Facades\Auth;
use MyResponse;
use Tr;

class CategoryController extends ContentController
{
    //protected $model;
    protected $breadcrumbs = [];

    public function __construct()
    {
        $this->breadcrumbs[] = ['title'=>Tr::_t('Головна'), 'url'=>'/'];
        $this->breadcrumbs[] = ['title'=>Tr::_t('Категоріі'), 'url'=>route('category')];
    }

    public function index(){
        $this->Content = TypeCategory::all();

        return MyResponse::good([
            'Content' => $this->Content,
            'PageView' => 'Category',
            'breadcrumbs' => $this->breadcrumbs,
        ]);
    }
    public function showC(TypeCategory $category){

        $this->Content = $category->model::published()->get();
        $this->breadcrumbs[] = ['title'=>Tr::_t($category->title), 'url'=>route('category-one', $category)];
        $pageView = $category->url.'list';
        if($category->url == 'documents'){
            $pageView = 'presentationlist';
        }
        if($category->url == 'book'){
            $pageView = 'fileslist';
        }
        return MyResponse::good([
            'Content' => $this->Content,
            'PageView' => $pageView,
            'breadcrumbs' => $this->breadcrumbs,
        ]);

    }
    public function showoneC(TypeCategory $category, $url){

        $this->Content = $category->model::where('url', $url)->first();
        $this->breadcrumbs[] = ['title'=>Tr::_t($category->title), 'url'=>route('category-one', $category)];
        $this->breadcrumbs[] = ['title'=>Tr::_t( $this->Content->title), 'url'=>route('category-one-content', ['category'=>$category, 'url'=>$this->Content])];
        $pageView = $category->url;
        if($category->url == 'documents'){
            $pageView = 'presentation';
        }
        if($category->url == 'book'){
            $pageView = 'file';
        }
        return MyResponse::good([
            'Content' => $this->Content,
            'PageView' => $pageView,
            'breadcrumbs' => $this->breadcrumbs,
        ]);
    }
    public function likeC(TypeCategory $category, $url){
        return response()->json($category->model::where('url', $url)->first()->toogleLike());
    }

    public function commentC(TypeCategory $category, $url){
        $this->Content = $category->model::where('url', $url)->first();
        return MyResponse::good([
            'Coments' => $this->Content->comments->load(['user']),
        ]);
    }
    public function addCommentC(Request $request, TypeCategory $category, $url){
        $this->Content = $category->model::where('url', $url)->first();
        $this->Content->comments()->save(new Comment(['text' => $request->text, 'user_id' => Auth::user()->id]));

        return MyResponse::good([
            'Coments' => $this->Content->comments->load(['user']),
        ]);
    }
}
