<?php

namespace App\Http\Controllers;

use App\Subscribe;
use Illuminate\Http\Request;
use Validator;
use MyResponse;

class SubscribeController extends Controller
{
    private $messages = [
        'required'    => 'Введіть будь-ласка свій Email',
        'unique'    => 'Ваш Email вже підписаній на новини',
        'max' => 'Email занадто довгий, напевно якась помилка',
        'email' => 'Введений текст не є Email-адресою',
    ];

    public function subscribe(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:subscribes|max:50',
        ], $this->messages);
        $ErrorCode = 1;
        $DebugMessage = 'Ви успішно підписались на оновлення';

        if ($validator->fails()) {
            $ErrorCode = 2;
            $DebugMessage = $validator->messages();

        }else {
            Subscribe::create(['email' => $request->input('email')]);
        }
        return MyResponse::good([
            'ErrorCode' => $ErrorCode,
            'DebugMessage' =>  $DebugMessage
        ]);
    }
}
