<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class Bisiness extends Model
{
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'data' => 10,
        ],
    ];
    protected $casts = [
        'data' => 'object',
        'rospisanie' => 'object',
    ];
    protected $appends = ['locatonToYandex', 'routeURL', 'cityName', 'shortDescription', 'rate'];

    public function getLocatonToYandexAttribute(){
        return $this->data->location->lat .','. $this->data->location->lng;
    }
    public function setLocatonToYandexAttribute($value)
    {
        $location = explode(",", $value);
        $data=$this->data;
        $data->location->lat = floatval($location[0]);
        $data->location->lng = floatval($location[1]);
        $this->data = $data;
        return $this;
    }
    public function getCityNameAttribute(){
        return $this->locale->name ?? '';
    }
    public function getRouteURLAttribute(){

        return route('bussines', $this);
    }
    public function getLocaleIdAttribute(){
        return $this->data->locale_id ?? null;
    }
    public function locale(){
        return $this->hasOne('App\City', 'id', 'locale_id');
    }
    public function getCategoryAttribute(){
        return $this->data->category ?? null;
    }
    public function category(){
        return $this->hasOne(CategoryBisiness::class, 'id', 'category');
    }
    public function getShortDescriptionAttribute(){
        //$value =

        return str_limit(strip_tags($this->data->description), 200);
    }
    public function rate(){
        return $this->morphToMany('App\Rate', 'rategable', 'retesgables');
    }
    public function setRate($value){
        if(Auth::check()) {
            $rate = $this->rate()->where('user_id', Auth::user()->id)
                ->orWhere('token', session()->get('_token'))
                ->get()->first();
            if($rate){
                $rate->value = $value;
                $rate->save();
            }else{
                $r = new Rate(['value'=>$value]);
                $r->save();
                $this->rate()->attach($r);
                Auth::user()->rate()->save($r);
            }
        }else{
            //dd($this->likes);
            $rate = $this->rate()->where('token', session()->get('_token'))->get()->first();
            if($rate){
                $rate->value = $value;
                $rate->save();
            }else{
                $this->rate()->create( ['token' => session()->get('_token'), 'value'=>$value]);
            }
        }
        return $this->getRateAttribute();
    }
    public function getRateAttribute(){
        $ret = new \stdClass();
        $ret->count = $this->rate()->avg('value');
        if(Auth::check()) {
            $if_I = $this->rate()->where('user_id', Auth::user()->id)
                ->orWhere('token', session()->get('_token'))
                ->get()->first();
            if($if_I){
                $ret->if_I = $if_I->value;
            }else{
                $ret->if_I = null;
            }
        }else{
            $ret->if_I = $this->rate()->where('token', session()->get('_token'))->get()->first();
        }
        $ret->comand = $this->getRouteRate();
        return $ret;
    }
    public function getRouteRate(){
        return route('bussines', $this);
    }
    public function scopePublished($query){
        return $query->where('publish', true)->where((function ($query) {
            $query->where('publish_at', '<', Carbon::now())
                ->orWhere('publish_at', '=', null);
        }));
    }
    public function comments(){
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function author(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
