<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoForStaticPage extends Model
{
    protected $fillable = ['key'];
    public function seo()
    {
        return $this->morphOne('App\SeoData', 'seo_datatable');
    }
}
