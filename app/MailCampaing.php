<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailCampaing extends Model
{
    public function tasks(){
        return $this->hasMany(MailTask::class, 'mail_compaing_id');
    }
    public function UTM(){
        return $this->hasMany(UTMLink::class, 'mail_compaing_id');
    }
}
