<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpDeskMessage extends Model
{
    public $casts = ['files' => 'array'];

    public function user(){
        return $this->belongsTo(User::class,  'user_id', 'id');
    }

    public function departure(){
        return $this->belongsTo(Specialist::class,'departure_id','id');
    }
}
