<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tr;
class Setting extends Model
{
    private static $setting;
    public $timestamps = false;
    protected $fillable = ['name'];

    private static $defautLocale;
    private static $needTranslate = false;

    protected function toTranslate($string){;
        if(!self::$defautLocale) {
            self::$defautLocale = config('translation.default_locale');
            if (app()->getLocale() !== self::$defautLocale) {
                self::$needTranslate = true;
            }
        }
        if(self::$needTranslate){
            return Tr::_t($string);
        }else{
            return $string;
        }
    }

    public static function getAll(){
        if(!self::$setting) {
            self::$setting = Setting::all()->flatMap(function ($values) {
                return [$values->name => $values->value];
            });
        }
        return self::$setting;
    }
    public static function getSetting(string $arg){
        if(self::getAll()->has($arg)){
            return self::getAll()[$arg];
        }else{
            Setting::firstOrCreate(['name' => $arg]);
        };
        return '';
    }
    public function getValueAttribute($value){
        if(gettype($value) == 'string'){
            return $this->toTranslate($value);
        }
        else{
            return $value;
        }
    }
}
