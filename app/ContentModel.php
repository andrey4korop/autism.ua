<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;
use Share;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \stdClass;
use App\Helpers\TranslateHelper as Tr;
use App\Tag;
use Nicolaslopezj\Searchable\SearchableTrait;

abstract class ContentModel extends Model
{
    use SoftDeletes;
    use \Spatie\Tags\HasTags;
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'title' => 10,
            'content' => 10,
        ],
    ];

    protected $routeName;
    protected $dates = ['deleted_at'];
    protected $appends = ['contentShort', 'routeURL', 'social', 'counts']; //, 'setting'
    protected $hidden = ['publish', 'deleted_at', 'author_id'];
    private static $defautLocale;
    private static $needTranslate = false;

   /* private $settin = [
        'seeNews' => true,
        'disableComments' => false,
        'disableAds' => false,
        'sidebarInterestingNow' => true,
        'sidebarPopularNow' => true,
        'sidebarComentNow' => true,
        'sidebarButtonNewMaterial' => true,
        'sidebarBlockPodpiska' => true,
    ];

    public function getSettingAttribute(){

        return $this->settin;
    }*/
    public static function getTagClassName(): string
    {
        return Tag::class;
    }

    protected function toTranslate($string){
        if(!self::$defautLocale) {
            self::$defautLocale = config('translation.default_locale');
            if (app()->getLocale() !== self::$defautLocale) {
                self::$needTranslate = true;
            }
        }
        if(self::$needTranslate){
            return Tr::_t($string);
        }else{
            return $string;
        }
    }

    public function getContent(){
        return $this;
    }

    protected function getContentShortAttribute(){
        $value = str_limit(strip_tags($this->content), 210, '[...]');
            return $value;
    }

    protected function getRouteURLAttribute(){
        return route($this->routeName.'-one',$this);
    }

    protected function getSocialAttribute(){
        return [
          'facebook' => Share::load($this->routeURL, $this->title, $this->main_img)->facebook(),
          'email' => Share::load($this->routeURL, $this->title, $this->main_img)->email(),
          'whatsapp' => Share::load($this->routeURL, $this->title, $this->main_img)->whatsapp(),
          'viber' => Share::load($this->routeURL, $this->title, $this->main_img)->viber(),
        ];
    }

    public function save(array $options = []){
        if(!$this->url && !in_array('url', $this->hidden)){
            $this->slug();
        }
        parent::save($options);
    }

    public function slug(){
        $this->url = Str::slug($this->title);
    }

    public function getRouteKeyName(){
        return 'url';
    }
    public function getRouteLike(){
        return route($this->routeName.'-like', $this);
    }
    public function getLikeAttribute(){
        $ret = new stdClass();
        $ret->count = $this->likes()->count();
        if(Auth::check()) {
            $ret->if_I = $this->likes()->where('user_id', Auth::user()->id)
                                    ->orWhere('token', session()->get('_token'))
                ->count();
        }else{
            $ret->if_I = $this->likes()->where('token', session()->get('_token'))->count();
        }
        $ret->comand = $this->getRouteLike();
        return $ret;
    }

    public function likes(){
        return $this->morphToMany('App\Like', 'likegable');
    }

    public function toogleLike(){
        if(Auth::check()) {
            $like = $this->likes()->where('user_id', Auth::user()->id)
                                ->orWhere('token', session()->get('_token'))
                ->get();
            if($like->count()){
                $this->likes()->detach($like);
                Like::destroy($like->pluck('id'));
            }else{
                $l = new Like();
                $l->save();
                $this->likes()->attach($l);
                Auth::user()->likes()->save($l);
            }
        }else{
            //dd($this->likes);
            $like = $this->likes()->where('token', session()->get('_token'))->get();
            if($like->count()){
                $this->likes()->detach($like);
                Like::destroy($like->pluck('id'));
            }else{
                $this->likes()->create( ['token' => session()->get('_token')]);
            }
        }
       return $this->getLikeAttribute();
    }
    public function see(){
        return $this->morphOne(See::class, 'countgable');
    }
    public function addSee(){
        if(!$this->see){
            $this->see()->save(new See(['count'=>0]));
        }
        $this->see->count++;
        $this->see->save();
        return $this;
    }
    public function getSee(){
        if(!$this->see){
            $this->see()->save(new See(['count'=>0]));
        }
        return $this->see()->first()->count;
    }
    public function getCountsAttribute(){
        $ret = new stdClass();
        $ret->like = $this->getLikeAttribute();
        $ret->see = $this->getSee();
        $ret->coment = $this->comments()->count();
        return $ret;
    }

    public function getMainImgAttribute($value){
        if($value) {
            return url($value);
        }else{
            return null;
        }
    }

    public function getTitleAttribute($value){
        return $this->toTranslate($value);
    }
    public function getContentAttribute($value){
        return $this->toTranslate($value);
    }

    public function seo()
    {
        return $this->morphOne('App\SeoData', 'seo_datatable');
    }
    public function scopePublished($query){
        return $query->where('publish', true)->where((function ($query) {
            $query->where('publish_at', '<', Carbon::now())
                ->orWhere('publish_at', '=', null);
        }));
    }
    public function author(){
        return $this->belongsTo('App\User', 'author_id');
    }
    public function comments(){
        return $this->morphMany(Comment::class, 'commentable');
    }

    private static $mod = [News::class, Blog::class, Calendar::class, ItIsImportant::class, UserHistory::class];
    public static function getLastContent(){
        $colect = collect([]);
        foreach (self::$mod as $model){
            $colect->push($model::published()->where('created_at', '>', Carbon::now()->subDay(100) )->get());
        }
        return $colect->collapse();
    }
}
