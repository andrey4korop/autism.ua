<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContentModel;

class UserMaterial extends ContentModel
{
    protected $routeName = '';
    protected $fillable = ['title', 'content', 'main_img', 'category_user_material_id'];

    public function category(){
        return $this->hasOne('App\CategoryUserMaterial', 'id', 'category_user_material_id');
    }
}
