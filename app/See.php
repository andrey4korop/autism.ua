<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class See extends Model
{
    public $timestamps = false;

    public $fillable = ['count'];
}
