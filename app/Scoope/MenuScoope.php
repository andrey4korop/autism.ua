<?php
/**
 * Created by PhpStorm.
 * User: Илюша
 * Date: 10.06.2018
 * Time: 6:28
 */

namespace App\Scoope;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class MenuScoope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $scoopeValue = '';
        switch (get_class ($model)){
            case 'App\LeftMenu':
                $scoopeValue = 'Left';
                break;
            case 'App\TopMenu':
                $scoopeValue = 'Top';
                break;
            case 'App\BottomMenu':
                $scoopeValue = 'Bottom';
                break;
        }
        $builder->where('category', '=', $scoopeValue);
    }
}