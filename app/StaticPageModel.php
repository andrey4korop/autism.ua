<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticPageModel extends Model
{
    public $timestamps = false;
    protected $appends = ['routeURL'];

    protected function getRouteURLAttribute(){
        return $this->url;
    }
}
