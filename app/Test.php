<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Nicolaslopezj\Searchable\SearchableTrait;
class Test extends Model
{
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'title' => 10,
        ],
    ];
    protected $casts = ['data' => 'object'];
    protected $appends = ['routeURL'];

    public function slug(){
        $this->url = Str::slug($this->title);
    }

    public function save(array $options = []){
        if(!$this->url && !in_array('url', $this->hidden)){
            $this->slug();
        }
        parent::save($options);
    }
    public function getRouteKeyName(){
        return 'url';
    }
    protected function getRouteURLAttribute(){
        return route('test-one',$this);
    }
    public function scopePublished($query){
        return $query->where('publish', true)->where((function ($query) {
            $query->where('publish_at', '<', Carbon::now())
                ->orWhere('publish_at', '=', null);
        }));
    }
}
