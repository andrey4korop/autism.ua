<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\TranslateHelper as Tr;

class MainLangString extends Model
{
    private static $strings;
    public $timestamps = false;


    private static $defautLocale;
    private static $needTranslate = false;

    protected static function toTranslate($string){
        //dump(self::$defautLocale);
        if(!self::$defautLocale) {

            self::$defautLocale = \App::make('config')->get('translation.default_locale');
            if (\App::getLocale() !== self::$defautLocale) {
                self::$needTranslate = true;
            }
        }
        //dump(\App::getLocale());
        if(self::$needTranslate){
            //dump(Tr::_t($string));
            return Tr::_t($string);
        }else{
            return $string;
        }
    }

    public static function getAll(){
        if(!self::$strings) {
            self::$strings = MainLangString::all()->flatMap(function ($values) {
                return [$values->shortCode => self::toTranslate($values->text)];
            });
        }
        return self::$strings->toArray();
    }
}
