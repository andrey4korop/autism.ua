<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    public $timestamps = false;

    protected $fillable =['token', 'user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
