<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon;

class CabinetCategory extends Model
{
    public $timestamps = false;
    public $appends = ['routeUrl'];

    public function getRouteKeyName(){
        return 'url';
    }
    public function slug(){
        $this->url = Str::slug($this->title);
    }
    public function getRouteURLAttribute(){
        return route('oneCabinets', $this);
    }
    public function save(array $options = []){
        if(!$this->url){
            $this->slug();
        }
        parent::save($options);
    }

    public function spesialist(){
        return $this->belongsToMany(Specialist::class, 'cabinet_categories_specialists');
    }
    public function scopePublished($query){
        return $query->where('publish', true)->where((function ($query) {
            $query->where('publish_at', '<', Carbon::now())
                ->orWhere('publish_at', '=', null);
        }));
    }
}
