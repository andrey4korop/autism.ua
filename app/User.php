<?php

namespace App;

use App\Mail\ResetPasswordSnipped;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['photoURL', 'roles'];

    protected $casts = ['setting'=>'array'];

    private $valideLang = ['uk', 'ru', 'en'];

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function rate()
    {
        return $this->hasMany('App\Rate');
    }

    public function photo(){
        return $this->hasOne('App\UserPhoto');
    }
    public function getPhotoUrlAttribute(){
        return $this->photo->url;
    }
    public function getRolesAttribute(){
        return $this->role->map(function ($item){
            return $item->role;
        });
    }
    /*public function setPhotoUrlAttribute($val){
        if($this->photo())
    }*/
    public function role(){
        return $this->belongsToMany(Role::class, 'user_role','user_id', 'role_id');
    }
    public function specialist(){
        return $this->hasOne(Specialist::class, 'user_id', 'id');
    }

    public function MyHelpdesk(){
        return $this->hasMany(HelpDeskRequest::class, 'user_id', 'id');
    }

    public function isAdmin(){
        if($this && $this->role->where('role' , 'admin')->count()){
            return true;
        }
        return false;
    }
    public function changeLang($request){
        if(in_array($request->lang, $this->valideLang)){
            $setting = $this->setting;
            $setting['lang'] =  $request->lang;
            $this->setting = $setting;
            $this->save();
            session(['lang' => $request->lang]);
        }
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordSnipped($token));
    }
}
