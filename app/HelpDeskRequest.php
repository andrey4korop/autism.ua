<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpDeskRequest extends Model
{
    public $casts = ['files' => 'array'];

    public function user(){
        return $this->belongsTo(User::class,  'user_id', 'id');
    }

    public function message(){
        return $this->hasMany(HelpDeskMessage::class, 'parent_id', 'id');
    }
}
