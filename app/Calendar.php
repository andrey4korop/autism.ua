<?php

namespace App;

use App\ContentModel;
use App\Helpers\TranslateHelper as Tr;

class Calendar extends ContentModel
{
    protected $routeName = 'calendar';

    public function getLocationAttribute($value){
        return $this->toTranslate($value);
    }
}
