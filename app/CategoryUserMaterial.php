<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryUserMaterial extends Model
{
    public $timestamps = false;
}
