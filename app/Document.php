<?php

namespace App;

class Document extends ContentModel
{
    protected $appends = [ 'routeURL', 'social', 'counts', 'file'];
    protected $routeName = 'documents';

    protected function getRouteURLAttribute(){
        return route('category-one-content',['category'=>'documents', 'url'=>$this]);
    }
    public function getRouteLike(){
        return route('category-like', ['category'=>'documents', 'url'=>$this]);
    }
    public function getFileAttribute(){
        return url($this->path);
    }
}
