<?php

namespace App;
use DB;
use Illuminate\Support\Str;
use Spatie\EloquentSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Spatie\EloquentSortable\SortableTrait;
use Illuminate\Database\Eloquent\Collection as DbCollection;
use Spatie\Tags\HasSlug;
use Tr;
class Tag extends Model implements Sortable
{
    use SortableTrait;
    protected $appends = ['routeURL', 'tagName', 'count'];
    public static $models;

    public $guarded = [];

    private static $defautLocale;
    private static $needTranslate = false;


    protected function toTranslate($string){;
        if(!self::$defautLocale) {
            self::$defautLocale = config('translation.default_locale');
            if (app()->getLocale() !== self::$defautLocale) {
                self::$needTranslate = true;
            }
        }
        if(self::$needTranslate){
            return Tr::_t($string);
        }else{
            return $string;
        }
    }

    public function scopeWithType(Builder $query, string $type = null): Builder
    {
        if (is_null($type)) {
            return $query;
        }

        return $query->where('type', $type)->orderBy('order_column');
    }

    /**
     * @param array|\ArrayAccess $values
     * @param string|null $type
     * @param string|null $locale
     *
     * @return \Spatie\Tags\Tag|static
     */
    public static function findOrCreate($values, string $type = null, string $locale = null)
    {
        $tags = collect($values)->map(function ($value) use ($type, $locale) {
            if ($value instanceof Tag) {
                return $value;
            }

            return static::findOrCreateFromString($value, $type, $locale);
        });

        return is_string($values) ? $tags->first() : $tags;
    }

    public static function getWithType(string $type): DbCollection
    {
        return static::withType($type)->orderBy('order_column')->get();
    }

    protected static function findOrCreateFromString(string $name, string $type = null, string $locale = null): Tag
    {
        $tag = static::findFromString($name, $type);

        if (!$tag) {
            $tag = static::create([
                'name' => $name,
                'slug' => Str::slug($name),
                'type' => $type,
            ]);
        }

        return $tag;
    }

    public function getRouteKeyName(){
        return 'slug';
    }
    protected function getRouteURLAttribute(){
        return route('tag', $this);
    }
    protected function getTagNameAttribute(){
        return $this->toTranslate($this->name);
    }
    public static function findFromString(string $name, string $type = null, string $locale = null)
    {
        $locale = $locale ?? app()->getLocale();

        return static::query()
            // ->where("name->{$locale}", $name)
            ->where('type', $type)->get()->filter(function ($item) use ($locale, $name){
                //dump($item);
                return $item->name == $name;
            })
            ->first();
    }
    public function oneItem($model)
    {
        return $this->morphedByMany($model, 'taggable')->get();
    }
    public function oneCount($model)
    {
        return $this->morphedByMany($model, 'taggable')->count();
    }
    public function items(){
        if(!self::$models) {
            self::$models = DB::table('taggables')->groupBy('taggable_type')->select('taggable_type')->get()->pluck('taggable_type');
        }
        $collect = collect();
        foreach (self::$models as $model){
            $collect->push($this->oneItem($model));
        }
        //dd($collect->collapse());
        return $collect->collapse();
    }
    public function getCountAttribute(){
        if(!self::$models) {
            self::$models = DB::table('taggables')->groupBy('taggable_type')->select('taggable_type')->get()->pluck('taggable_type');
        }
        $count = 0;
        foreach (self::$models as $model){
            $count += $this->oneCount($model);
        }
        return $count;
    }
    public static function getPopularTag(){
        if(!self::$models) {
            self::$models = DB::table('taggables')->groupBy('taggable_type')->select('taggable_type')->get()->pluck('taggable_type');
        }
        $allTags = self::all();
        $sortTags = $allTags->sortByDesc('count')->values();
        return $sortTags;
    }
}
