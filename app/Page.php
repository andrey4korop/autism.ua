<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContentModel;

class Page extends ContentModel
{
    protected $routeName = 'page';
    public $casts = ['setting' => 'array'];
}
