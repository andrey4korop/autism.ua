<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use App\Scoope\MenuScoope;
use Tr;
class Menu extends Model
{
    use NodeTrait;

    private static $defautLocale;
    private static $needTranslate = false;

    protected $appends = ['routeURL'];
    protected $hidden = ['category', 'created_at', 'menutable', 'menutable_id', 'menutable_type', 'updated_at', '_lft', '_rgt'];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new MenuScoope);
    }

    protected function toTranslate($string){;
        if(!self::$defautLocale) {
            self::$defautLocale = config('translation.default_locale');
            if (app()->getLocale() !== self::$defautLocale) {
                self::$needTranslate = true;
            }
        }
        if(self::$needTranslate){
            return Tr::_t($string);
        }else{
            return $string;
        }
    }

    public function menutable()
    {
        return $this->morphTo();
    }
    protected function getRouteURLAttribute(){
        return $this->menutable->routeURL;
    }

    public function scopeTop($query, $type){
        return $query->where('category', 'Top');
    }

    public function scopeLeft($query, $type){
        return $query->where('category', 'Left');
    }
    public function getTitleAttribute($value){
        return $this->toTranslate($value);
    }
}
