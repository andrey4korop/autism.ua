<?php

namespace App;

use App\ContentModel;

use Alaouy\Youtube\Facades\Youtube;
class Video extends ContentModel
{
    protected $appends = [ 'routeURL', 'social', 'counts'];
    protected $routeName = 'video';

    protected function getRouteURLAttribute(){
        return route('category-one-content',['category'=>'video', 'url'=>$this]);
    }
    public function getRouteLike(){
        return route('category-like', ['category'=>'video', 'url'=>$this]);
    }

    public function save(array $options = []){
        $this->youtube_id = Youtube::parseVidFromURL($this->youtube_url);
        $youtube = Youtube::getVideoInfo($this->youtube_id);
        if(!$this->main_img) {
            if(property_exists($youtube->snippet->thumbnails, 'maxres')) {
                $this->main_img = $youtube->snippet->thumbnails->maxres->url;
            }else{
                $this->main_img = $youtube->snippet->thumbnails->standard->url;
            }
        }
        if(!$this->title) {
            $this->title = $youtube->snippet->title;
        }
        if(!$this->url){
            $this->slug();
        };
        parent::save($options);
    }
}
