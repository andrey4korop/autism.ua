<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;
class FaqCategory extends Model
{
    use SearchableTrait;
    protected $searchable = [
        'columns' => [
            'title' => 10,
        ],
    ];
    public $appends = ['routeUrl', 'faqsCount'];

    public function getRouteKeyName(){
        return 'url';
    }
    public function slug(){
        $this->url = Str::slug($this->title);
    }
    public function getRouteURLAttribute(){
        return route('faq-one', $this);
    }
    public function save(array $options = []){
        if(!$this->url){
            $this->slug();
        }
        parent::save($options);
    }
    public function faqs(){
        return $this->hasMany(FaqPage::class, 'category_id', 'id')->published();
    }
    public function scopePublished($query){
        return $query->where('publish', true)->where((function ($query) {
            $query->where('publish_at', '<', Carbon::now())
                ->orWhere('publish_at', '=', null);
        }));
    }
    public function getFaqsCountAttribute(){
        return $this->faqs->count();
    }
}
