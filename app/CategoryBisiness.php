<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CategoryBisiness extends Model
{
    public $casts = ['data'  => 'array'];
    public function scopePublished($query){
        return $query->where('publish', true)->where((function ($query) {
            $query->where('publish_at', '<', Carbon::now())
                ->orWhere('publish_at', '=', null);
        }));
    }
}
