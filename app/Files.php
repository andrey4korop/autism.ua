<?php

namespace App;


class Files extends ContentModel
{
    protected $searchable = [
        'columns' => [
            'title' => 10,
        ],
    ];
    protected $appends = [ 'routeURL', 'file'];
    protected $hidden = ['url'];
    protected $routeName = 'presentation';

    public function getRouteKeyName(){
        return 'id';
    }
    protected function getRouteURLAttribute(){
        return route('category-one-content',['category'=>'files', 'url'=>$this]);
    }
    public function getRouteLike(){
        return route('category-like', ['category'=>'files', 'url'=>$this]);
    }
    public function getFileAttribute(){
        return url($this->path);
    }
}
