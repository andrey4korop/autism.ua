<?php

namespace App\Helpers;

use Stevebauman\Translation\Contracts\Client;
use Pupo4ek\YandexTranslator\TranslatorFactory;

class YandexClient implements Client
{
    /** @var TranslateClient */
    protected $client;

    protected $source;
    protected $target;
    private $maxLength = 7000;
    public function __construct()
    {

        $this->client =  TranslatorFactory::create(env('TRANSLATE_API_KEY'));

    }
    /**
     * Set source language.
     *
     * @param string $source Language code
     *
     * @return mixed
     */
    public function setSource($source = null)
    {
        $this->source = $source;
    }

    /**
     * Set target language.
     *
     * @param string $target Language code
     *
     * @return mixed
     */
    public function setTarget($target)
    {
       $this->target=$target;
    }
    private function getArrayString($string){
        $arr=[];
        $pos = mb_strpos($string, '.', $this->maxLength);
        $string2 = mb_substr($string, $pos+1);
        $arr[] = mb_substr($string, 0, $pos+1);
        if(mb_strlen($string2)>$this->maxLength){
            $arr = array_flatten([$arr, $this->getArrayString($string2)]);
        }else{
            $arr[] = $string2;
        }
        return $arr;
    }

    private function concatTranslateString($array){
        $returnString = '';
        foreach ($array as $string){
            $s = $this->translate($string);
            $returnString.= $s;
        }
        return $returnString;
    }
    /**
     * Translate the text.
     *
     * @param string $text
     *
     * @return mixed
     */
    public function translate($text)
    {
        //dd( app()->getLocale());
        if(!$this->target){
            $this->target = app()->getLocale();
        }
        if(mb_strlen($text)>$this->maxLength+2000) {

            return $this->concatTranslateString($this->getArrayString($text));
        }
       return $this->client->translate($text, $this->target);
    }
}
