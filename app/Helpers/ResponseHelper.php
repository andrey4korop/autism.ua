<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 15.07.2018
 * Time: 21:51
 */

namespace App\Helpers;

use App\MainLangString;
use App\Setting;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Share;
use SEO;
use SEOMeta;
use OpenGraph;
class ResponseHelper
{
    private  $responseArray = [];

    private function __construct(){
        $adminPanelMenu = [];
        if(Auth::check() && Auth::user()->isAdmin()){
            $adminPanelMenu[] = ['title'=> 'Админ-панель', 'url'=> '/admin'];
            $adminPanelMenu[] = ['title'=> 'HelpDesk', 'url'=> '/helpdesk'];
        }
        $this->responseArray = [
            'Title'=>'',
            'ErrorCode' => 1,
            'DebugMessage' => '',
            'ListContent' => [],
            'Content' => [],
            'btnReadAll' => [],
            'menu' => [],
            'PageView' => '',
            'MainPage' => [
                'facebook' => Share::load(route('home'), '')->facebook(),
                'email' => Share::load(route('home'), '')->email(),
                'whatsapp' => Share::load(route('home'), '')->whatsapp(),
                'viber' => Share::load(route('home'), '')->viber(),
            ],
            'user' => Auth::user(),
            'setting' => Setting::getAll(),
            'langString' => MainLangString::getAll(),
            'adminPanelMenu' => $adminPanelMenu,
            'SEO'=>[
                'title'=>'',
                'keywords'=>'',
                'description'=>''
            ],
            'session'=>session()->all(),
        ];

        $this->responseArray['menu']['Top'] = \App\TopMenu::all()->sortBy('order')->toTree();
        $this->responseArray['menu']['Left'] = \App\LeftMenu::all()->sortBy('order')->toTree();
        $this->responseArray['menu']['Bottom'] = \App\BottomMenu::all()->sortBy('order')->toTree();
    }

    /**
     * @param array $parametrs
     * @param array $addition
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public static function good(array $parametrs=[], array $addition=[]){
        $response = new self();
        return  $response->setParametrs($parametrs, $addition)->_send();
    }

    private function _send(){
        if(Request::ajax()) {
            return response()->json($this->responseArray);
        }else{
            $this->setSEO();
            return view('hidden', $this->responseArray);
        }
    }
    private function setSEO(){
        //dd(Request::fullUrl());
        $title = $title = $this->responseArray['Title'];
        $keywords = Setting::getSetting('defaut_seo_keywords');
        $description = Setting::getSetting('defaut_seo_description');
        $image = url('/img/logo.gif');
        try {
            if ($this->responseArray['SEO'] && $this->responseArray['SEO']['title']) {
                $title = $this->responseArray['SEO']['title'];
            }
        }catch (\Exception $e){}
        try {
            if ($this->responseArray['SEO'] && $this->responseArray['SEO']['description']) {
                $description = $this->responseArray['SEO']['description'];
            }
        }catch (\Exception $e){}
        try {
            if ($this->responseArray['SEO'] && $this->responseArray['SEO']['keywords']) {
                $keywords = $this->responseArray['SEO']['keywords'];
            }
        }catch (\Exception $e){}
        try {
            if ($this->responseArray['Content'] && count($this->responseArray['Content']) == 1 && class_basename($this->responseArray['Content']) != "Collection" && $this->responseArray['Content']->main_img) {
                $image = url($this->responseArray['Content']->main_img);
            }
        }catch (\Exception $e){}
        //SEO::opengraph()->addProperty('type', 'articles');
        //SEO::twitter()->setSite('@andrey4korop');
        SEO::setTitle($title);
        SEO::setDescription($description);
        OpenGraph::addImage($image);
        SEOMeta::setKeywords($keywords);
    }
    private function setParametrs($parametrs, $addition){
        if(!empty($parametrs)) {
            foreach ($parametrs as $key => $value) {
                $this->responseArray[$key] = $value;
            }
        }

        if(!empty($addition)) {
            foreach ($addition as $key => $value) {
                //$this->responseArray[$key] = $value;
                foreach ($value as $v){
                    $this->responseArray[$key][] = $v;
                }
            }
        }

        return $this;
    }
}