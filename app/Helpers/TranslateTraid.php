<?php

namespace App\Helpers;

use App\Helpers\TranslateHelper as Tr;

trait TranslateTraid
{
    private static $defautLocale;
    private static $needTranslate = false;
    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getAttributeValue($key)
    {
        if (!$this->isTranslatableAttribute($key)) {
            return parent::getAttributeValue($key);
        }

        return $this->getTranslation($key);
    }

    /**
     * @param string $key
     * @param string $locale
     *
     * @return mixed
     */
    public function translate(string $key)
    {
        return $this->getTranslation($key);
    }

    /***
     * @param string $key
     * @param string $locale
     * @param bool $useFallbackLocale
     *
     * @return mixed
     */
    public function getTranslation(string $key)
    {
        if(!self::$defautLocale) {
            self::$defautLocale = config('translation.default_locale');
            if (app()->getLocale() !== self::$defautLocale) {
                self::$needTranslate = true;
            }
        }
        if(self::$needTranslate){
            if($this->getAttributes()[$key]){
                return Tr::_t($this->getAttributes()[$key]);
            }
            return $this->getAttributes()[$key];
        }else{
            return $this->getAttributes()[$key];
        }
    }
    protected function getLocale() : string
    {
        return config('app.locale');
    }
    public function isTranslatableAttribute(string $key) : bool
    {
        return in_array($key, $this->getTranslatableAttributes());
    }
    public function getTranslatableAttributes() : array
    {
        return is_array($this->translatable)
            ? $this->translatable
            : [];
    }

}