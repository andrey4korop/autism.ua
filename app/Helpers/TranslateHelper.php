<?php
/**
 * Created by PhpStorm.
 * User: Илюша
 * Date: 09.06.2018
 * Time: 13:33
 */

namespace App\Helpers;

use App;
use Translation;

class TranslateHelper
{
    public static function _t($text, $toLocale=''){
        if(!$toLocale){
            $toLocale = App::getLocale();
        }
        if($toLocale == 'uk'){
            return $text;
        }
        if(!$text){
            return $text;
        }
        $LocaleString = Translation::translate($text, [], $toLocale);
        if(gettype($LocaleString) == 'object'){
            $LocaleString = $LocaleString->getResult()[0];
        }
        return $LocaleString;
    }
}
