<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

class RootModel extends Model
{
    public $timestamps = false;
    protected $appends = ['routeURL'];

    protected function getRouteURLAttribute(){
        return '#';
    }
}
