<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailTask extends Model
{
    protected $fillable = ['email', 'mail_compaing_id'];
    public function compaing(){
        return $this->belongsTo(MailCampaing::class, 'id', 'mail_compaing_id');
    }
}
