<?php

namespace App;

class Book extends ContentModel
{
    protected $searchable = [
        'columns' => [
            'title' => 10,
        ],
    ];
    protected $appends = [ 'routeURL', 'file'];
    protected $hidden = ['url'];
    protected $routeName = 'book';

    public function getRouteKeyName(){
        return 'id';
    }
    protected function getRouteURLAttribute(){
        return route('category-one-content',['category'=>'book', 'url'=>$this]);
    }
    public function getRouteLike(){
        return route('category-like', ['category'=>'book', 'url'=>$this]);
    }
    public function getFileAttribute(){
        return url($this->path);
    }
}
