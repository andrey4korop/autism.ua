<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $fillable = ['data_before', 'data_after', 'user_id', 'model'];

    protected $casts = ['data_before' => 'array', 'data_after' => 'array'];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
