<?php

namespace App;

use App\ContentModel;

class Presentation extends ContentModel
{
    protected $appends = [ 'routeURL', 'social', 'counts', 'file'];
    protected $routeName = 'presentation';

    protected function getRouteURLAttribute(){
        return route('category-one-content',['category'=>'presentation', 'url'=>$this]);
    }
    public function getRouteLike(){
        return route('category-like', ['category'=>'presentation', 'url'=>$this]);
    }
    public function getFileAttribute(){
        return url($this->path);
    }
}
