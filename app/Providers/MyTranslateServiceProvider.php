<?php

namespace App\Providers;

use App\Helpers\TranslateHelper;
use Illuminate\Support\ServiceProvider;

class MyTranslateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/TranslateHelper.php';
    }
}
