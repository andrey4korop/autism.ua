<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 15.07.2018
 * Time: 21:51
 */

namespace App\Providers;
use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/ResponseHelper.php';
    }

}