<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PodCategorii extends Model
{
    public $casts = ['data'  => 'array'];
}
