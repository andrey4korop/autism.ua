<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = 'contact_uses';

    private static $defautLocale;
    private static $needTranslate = false;
    protected function toTranslate($string){
        //dump(self::$defautLocale);
        if(!self::$defautLocale) {

            self::$defautLocale = \App::make('config')->get('translation.default_locale');
            if (\App::getLocale() !== self::$defautLocale) {
                self::$needTranslate = true;
            }
        }
        //dump(\App::getLocale());
        if(self::$needTranslate){
            //dump(Tr::_t($string));
            return Tr::_t($string);
        }else{
            return $string;
        }
    }
    public function seo()
    {
        return $this->morphOne('App\SeoData', 'seo_datatable');
    }
    public function getTitleAttribute($value){
        return $this->toTranslate($value);
    }
    public function getContentAttribute($value){
        return $this->toTranslate($value);
    }
}
