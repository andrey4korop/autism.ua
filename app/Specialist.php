<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialist extends Model
{
    protected $searchable = [
        'columns' => [
            'name' => 10,
            'description' => 7,
        ],
    ];
    protected $casts = ['data' => 'array'];

    public function category(){
        return $this->belongsToMany(CabinetCategory::class, 'cabinet_categories_specialists');
    }
    public function MyHelpdesk(){
        return $this->hasMany(HelpDeskRequest::class, 'departure_id', 'id');
    }
}
