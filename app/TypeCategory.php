<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCategory extends Model
{
    protected $routeName = 'url';
    protected $appends = [ 'routeURL'];

    public function getRouteURLAttribute(){
        if($this->url!='faq'){
            return route('category-one', $this);
        }else{
            return route('faq');
        }

    }
    public function getRouteKeyName(){
        return 'url';
    }
}
