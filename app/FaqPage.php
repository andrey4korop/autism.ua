<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqPage extends ContentModel
{
    protected $routeName = 'faqpage';
    protected $appends = ['contentShort', 'routeURL', 'social', 'counts'];
}
