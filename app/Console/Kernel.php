<?php

namespace App\Console;

use App\Mail\NewsSnipped;
use App\MailCampaing;
use App\MailTask;
use App\Subscribe;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
           $campain = MailCampaing::create();
           $subscribers = Subscribe::all();
           $arrayTasks = [];
           foreach ($subscribers as $subscriber){
               $arrayTasks[]= new MailTask(['email' => $subscriber->email]);
           }
           $campain->tasks()->saveMany($arrayTasks);

            MailTask::where('is_send', 0)->where('created_at', '<', Carbon::now()->subDay(30))->delete();
        })->weekly();

        $schedule->call(function () {
            $task = MailTask::where('is_send', 0)->inRandomOrder()->first();
            if($task) {
                $mail = new NewsSnipped($task);
                try {
                    Mail::to($task->email)
                        ->send($mail);
                    $task->is_send = true;
                    $task->save();
                } catch (\Exception $e) {
                }
            }
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
