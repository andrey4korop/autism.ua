<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UTMLink extends Model
{
    public $fillable =['redirect_link', 'mail_compaing_id', 'utm_link'];
    public function compaing(){
        return $this->belongsTo(MailCampaing::class, 'mail_compaing_id','id' );
    }
    public function getRouteKeyName()
    {
        return 'utm_link';
    }

    public static function getLink($url, $campaing){
        $utmLink = UTMLink::firstOrCreate(['redirect_link'=> $url, 'mail_compaing_id'=>$campaing], ['utm_link'=>str_random(40)]);
        return route('utm', $utmLink, true);
    }
    public function addSee(){
        $compaing = $this->compaing;
        $compaing->count_see++;
        $compaing->save();
    }
}
