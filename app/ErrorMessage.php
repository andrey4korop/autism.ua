<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ErrorMessage extends Model
{
    use SoftDeletes;
    public $fillable = ['url', 'text', 'coment'];
}
