<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::match(['get', 'post'],'/', 'MainController@index')->name('home');;
Route::get('/test', function () {

    $a =  new \App\Http\Controllers\SearchController();
    return $a->search('аутизм');
    /*function xmlToArray($xml, $options = array()) {
        $defaults = array(
            'namespaceSeparator' => ':',//you may want this to be something other than a colon
            'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(),   //array of xml tag names which should always become arrays
            'autoArray' => true,        //only create arrays for tags which appear more than once
            'textContent' => '$',       //key used for the text content of elements
            'autoText' => true,         //skip textContent key if node has no attributes or child nodes
            'keySearch' => false,       //optional search and replace on tag and attribute names
            'keyReplace' => false       //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces();
        $namespaces[''] = null; //add base (empty) namespace

        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch']) $attributeName =
                    str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                $attributeKey = $options['attributePrefix']
                    . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                    . $attributeName;
                $attributesArray[$attributeKey] = (string)$attribute;
            }
        }

        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = xmlToArray($childXml, $options);
                list($childTagName, $childProperties) = each($childArray);

                //replace characters in tag name
                if ($options['keySearch']) $childTagName =
                    str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                //add namespace prefix, if any
                if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

                if (!isset($tagsArray[$childTagName])) {
                    //only entry with this key
                    //test if tags of this type should always be arrays, no matter the element count
                    $tagsArray[$childTagName] =
                        in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                            ? array($childProperties) : $childProperties;
                } elseif (
                    is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                    === range(0, count($tagsArray[$childTagName]) - 1)
                ) {
                    //key already exists and is integer indexed array
                    $tagsArray[$childTagName][] = $childProperties;
                } else {
                    //key exists so convert to integer indexed array with previous value in position 0
                    $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                }
            }
        }

        //get text content of node
        $textContentArray = array();
        $plainText = trim((string)$xml);
        if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
            ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }
    $xmlNode = simplexml_load_file('E:\OSPanel\domains\z.xml');
    $arrayData = xmlToArray($xmlNode);
/*
 * autors
 foreach ($arrayData['rss']['channel']['wp:author'] as $autor){
        $photo = new \App\UserPhoto();
        $u = new \App\User();
        $u->id = $autor['wp:author_id'];
        $u->email = $autor['wp:author_email'];
        $u->name = $autor['wp:author_display_name'];
        $u->password = '';
        $u->save();
        $u->photo()->save($photo);
        $role = \App\Role::where('role', 'user')->first();
        $u->role()->attach($role);
    }
    * tabg
      foreach ($arrayData['rss']['channel']['wp:tag'] as $item){
        dump($item);
        $tag = new \App\Tag();
        $tag->id = $item['wp:term_id'];
        $tag->name = $item['wp:tag_name'];
        $tag->slug = $item['wp:tag_slug'];
        $tag->save();
    }
    foreach ($arrayData['rss']['channel']['item'] as $item) {
        if($item['wp:status'] != 'trash' && $item['wp:status'] != 'draft') {
            $keyed = collect($item['wp:postmeta'])->mapWithKeys(function ($i) {
                return [$i['wp:meta_key'] => $i['wp:meta_value']];
            });


            $blog = new \App\Blog();
            $blog->id = $item['wp:post_id'];
            $blog->title = $item['title'];
            $blog->url = $item['wp:post_name'];

            if (array_key_exists('_thumbnail_id', $keyed)) {
                $blog->main_img = $keyed['_thumbnail_id'];
            }
            // dump(gettype($item['content:encoded']));
            if(gettype($item['content:encoded']) == 'array'){
                dd($item);
            }
            $blog->content = $item['content:encoded'];
            $blog->author_id = \App\User::where('name', $item['dc:creator'])->first() ? \App\User::where('name', $item['dc:creator'])->first()->id : null;
            $blog->created_at = \Carbon\Carbon::parse($item['wp:post_date']);
            $blog->publish = $item['wp:status'] == 'publish';
            $blog->save();
            if (array_key_exists('_yoast_wpseo_focuskw', $keyed)) {
                $seo = new \App\SeoData();
                $seo->keywords = $keyed['_yoast_wpseo_focuskw'];
                $blog->seo()->save($seo);
            }
        }
    }*/
    //dd($arrayData['rss']['channel']['item']);
    /*$keyed = collect($arrayData['rss']['channel']['item'])->mapWithKeys(function ($i) {
        return [$i['wp:post_id'] => $i['wp:attachment_url']];
    });
    foreach (\App\ItIsImportant::all() as $v){
        if($v->getOriginal('main_img')){
            $v->main_img = $keyed[$v->getOriginal('main_img')];
            $v->save();
        }
    }*/
   // dd($arrayData['Workbook']['Worksheet'][0]['ss:Table']['ss:Row']);

});
/*Route::get('{any}', function () {
    return view('main');
})->where('any', '^((?!admin).)*$');*/

Route::post('/uploadIMG', 'UploadImageController@upload');
//Auth
//Auth::routes();
Route::post('/register','\App\Http\Controllers\Auth\RegisterController@register')->name('register');
Route::post('/login','\App\Http\Controllers\Auth\LoginController@login')->name('login');
Route::post('/auth/facebook','\App\Http\Controllers\Auth\LoginController@facebook')->name('loginfacebook');
Route::get('/auth/facebook', function (){return '';});
Route::post('/auth/google','\App\Http\Controllers\Auth\LoginController@google')->name('loginfacebook');
Route::get('/auth/google', function (){return '';});
Route::post('/logout','\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::post('/password/email', '\App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//Route::get('/password/reset', '\App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/reset', '\App\Http\Controllers\Auth\ResetPasswordController@reset');
Route::match(['get', 'post'],'/password/reset/{token} ', '\App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::match(['get', 'post'],'/news','NewsController@show')->name('news');
Route::match(['get', 'post'],'/news/{url}','NewsController@showone')->name('news-one');
Route::post('/news/{url}/like','NewsController@like')->name('news-like');
Route::post('/news/{url}/comment','NewsController@comment')->name('news-comment');
Route::put('/news/{url}/comment','NewsController@addComment')->name('news-comment');

Route::match(['get', 'post'],'/blog','BlogController@show')->name('blog');
Route::match(['get', 'post'],'/blog/{url}','BlogController@showone')->name('blog-one');
Route::post('/blog/{url}/like','BlogController@like')->name('blog-like');
Route::post('/blog/{url}/comment','BlogController@comment')->name('blog-comment');
Route::put('/blog/{url}/comment','BlogController@addComment')->name('blog-comment');

Route::match(['get', 'post'],'/calendar','CalendarController@show')->name('calendar');
Route::match(['get', 'post'],'/calendar/{url}','CalendarController@showone')->name('calendar-one');
Route::post('/calendar/{url}/like','CalendarController@like')->name('calendar-like');
Route::post('/calendar/{url}/comment','CalendarController@comment')->name('calendar-comment');
Route::put('/calendar/{url}/comment','CalendarController@addComment')->name('calendar-comment');

Route::match(['get', 'post'],'/important','ItIsImportantController@show')->name('important');
Route::match(['get', 'post'],'/important/{url}','ItIsImportantController@showone')->name('important-one');
Route::post('/important/{url}/like','ItIsImportantController@like')->name('important-like');
Route::post('/important/{url}/comment','ItIsImportantController@comment')->name('important-comment');
Route::put('/important/{url}/comment','ItIsImportantController@addComment')->name('important-comment');

Route::match(['get', 'post'],'/history','UserHistoryController@show')->name('UserHistory');
Route::match(['get', 'post'],'/history/{url}','UserHistoryController@showone')->name('UserHistory-one');
Route::post('/history/{url}/like','UserHistoryController@like')->name('UserHistory-like');
Route::post('/history/{url}/comment','UserHistoryController@comment')->name('history-comment');
Route::put('/history/{url}/comment','UserHistoryController@addComment')->name('history-comment');

Route::match(['get', 'post'],'/category', 'CategoryController@index')->name('category');
Route::match(['get', 'post'],'/category/{category}', 'CategoryController@showC')->name('category-one');
Route::match(['get', 'post'],'/category/{category}/{url}', 'CategoryController@showoneC')->name('category-one-content');
Route::post('/category/{category}/{url}/like', 'CategoryController@likeC')->name('category-like');
Route::post('/category/{category}/{url}/comment','CategoryController@commentC')->name('category-comment');
Route::put('/category/{category}/{url}/comment','CategoryController@addCommentC')->name('category-comment');

Route::match(['get', 'post'],'/about-us', 'MainController@aboutUs')->name('aboutUs');
Route::match(['get', 'post'],'/contact-us', 'MainController@contactUs')->name('contactUs');
Route::put('/contact-us', 'MainController@contactUsPut')->name('contactUsPut');

Route::match(['get', 'post'], '/registraton-business', 'ServisesController@registratonBusiness')->name('registratonBusiness');
Route::put('/registraton-business', 'ServisesController@registratonBusiness2');

Route::match(['get', 'post'],'/servises', 'ServisesController@map')->name('servises');
Route::post('/servises/search', 'ServisesController@searchMap')->name('searchMap');
Route::match(['get', 'post'],'/servises/{id}', 'ServisesController@bissines')->name('bussines');
Route::put('/servises/{id}', 'ServisesController@setRate')->name('bussines');
Route::post('/servises/{id}/comment','ServisesController@commentC')->name('bussines-comment');
Route::put('/servises/{id}/comment','ServisesController@addCommentC')->name('bussines-comment');


Route::put('/subcribe', 'SubscribeController@subscribe')->name('subscribe');
Route::post('/getLastComments', 'InterestingNowController@getLastComents');

Route::match(['get', 'post'],'/suggest', 'SuggestController@index')->name('suggest');
Route::put('/suggest', 'SuggestController@save')->name('suggest');

Route::match(['get', 'post'],'/cabinets', 'Cabinet@index')->name('cabinets');
Route::match(['get', 'post'],'/cabinets/create', 'Cabinet@create');
Route::match(['get', 'post'],'/cabinets/{category}', 'Cabinet@oneCategory')->name('oneCabinets');
Route::match(['get', 'post'],'/cabinets/{category}/{id}', 'Cabinet@form')->name('form');


Route::match(['get', 'post'],'/help', 'Cabinet@helpDeskUserIndex');
Route::match(['get', 'post'],'/help/{id}', 'Cabinet@helpDeskOne');
Route::post('/help/{id}/sendMessage', 'Cabinet@sendMessageUser');

Route::match(['get', 'post'],'/helpdesk', 'Cabinet@helpDeskIndex')->name('helpdesk');
Route::match(['get', 'post'],'/helpdesk/{id}', 'Cabinet@helpDeskOne')->name('helpdeskOne');
Route::post('/helpdesk/{id}/sendMessage', 'Cabinet@sendMessage')->name('sendHelpdesk');

Route::match(['get', 'post'],'/faq', 'FaqController@index')->name('faq');
Route::match(['get', 'post'],'/faq/page/{url}', 'FaqController@page')->name('faqpage-one');
//Route::post('/faq/page/{url}/like', 'FaqController@like')->name('faqpage-like');
Route::match(['get', 'post'],'/faq/cabinets', 'FaqController@category');
Route::match(['get', 'post'],'/faq/cabinets/{category}', 'FaqController@oneCategory')->name('oneCabinets');
Route::match(['get', 'post'],'/faq/cabinets/{category}/{id}', 'Cabinet@form')->name('form');

Route::post('/faq/getPopular', 'FaqController@getPopular')->name('faqpage-like');
Route::post('/faq/getNew', 'FaqController@getNew');
Route::post('/faq/getCategory', 'FaqController@getCategory');
Route::match(['get', 'post'],'/faq/{url}', 'FaqController@one')->name('faq-one');




Route::post('/getPopularTags', 'MainController@getPopularTags');
Route::post('/getInterestingNow', 'InterestingNowController@getInterestingNow');
Route::match(['get', 'post'],'/hashtag/{tag}', 'MainController@getListItemFromTag')->name('tag');

Route::post('/changeLang', 'MainController@changeLang');
Route::post('/changeLang', 'MainController@changeLang');

Route::match(['get', 'post'],'/tests', 'TestController@index')->name('test');
Route::match(['get', 'post'],'/tests/{url}', 'TestController@one')->name('test-one');

Route::get('/utm/{link}', function (\App\UTMLink $link) {
    $link->addSee();
    return redirect($link->redirect_link);
})->name('utm');

Route::put('/sendError', 'MainController@sendError');

Route::post('/{url}/like','PageController@like')->name('page-like');
Route::post('/{url}/comment','PageController@comment')->name('page-comment');
Route::put('/{url}/comment','PageController@addComment')->name('page-comment');

Route::post('/search', 'SearchController@search')->name('search');

Route::match(['get', 'post'],'/{url}', 'PageController@showone')->name('page-one')->where('url', '^((?!admin).)*$');

//Route::get('/home', 'HomeController@index')->name('home');


